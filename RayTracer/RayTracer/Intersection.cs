﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Intersection
    {
        private Vector pInter, p0, normal;
        int mat;
        Double dist;

        public Intersection(Vector pInter, Vector p0, int mat, Vector normal) {
            this.pInter = pInter;
            this.mat = mat;
            this.p0 = p0;
            this.normal = normal;
            this.dist = Vector.cVector(p0, pInter).nVector();
        }

        public Vector getpInter()
        {
            return pInter;
        }

        public int getMat()
        {
            return mat;
        }

        public Double getDist()
        {
            return dist;
        }

        public Vector getNormal()
        {
            return this.normal;
        }

        public void setPInter(Vector pInter)
        {
            this.pInter = pInter;
            this.dist = Vector.cVector(p0, pInter).nVector();
        }
    }
}
