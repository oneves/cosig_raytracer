﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class RayTracer
    {

        private Scene scene;
        private Bitmap img;

        private static double epsilon = 0.000000001;
        private Boolean amb, shade, reflect, refract;
        private int iter;
        public double renderStatus;
        public Transformation[] transf;

        //Construtor
        public RayTracer(Scene scene, int iter, Boolean amb, Boolean shade, Boolean reflect, Boolean refract)
        {
            renderStatus = 0;
            this.scene = scene;
            this.amb = amb;
            this.reflect = reflect;
            this.refract = refract;
            this.shade = shade;
            this.iter = iter;
            transf = new Transformation[scene.getTrans().Count()];
        }

        //Calcula a determinante da matriz 3x3
        private static double determinant3x3(double a11, double a12, double a13, double a21, double a22, double a23, double a31, double a32, double a33)
        {
            return ((a11 * a22 * a33) + (a12 * a23 * a31) + (a13 * a21 * a32) - (a13 * a22 * a31) - (a12 * a21 * a33) - (a11 * a23 * a32));
        }

        //Multiplica todas as transformações com a transformação da camera 
        private void prepareTransformation()
        {
            int count1 = 0;
            foreach (Transformation t in scene.getTrans())
            {
                Transformation trans = new Transformation(t);
                if (count1 != 1)
                    trans.mult(scene.getTrans().ElementAt(1));
                transf[count1] = trans;
                count1++;
            }
            int test = transf.Count();
        }

        //transforma um certo raio na referência para todas as transformações
        private Ray[] calcRaysTrasnf(Vector dir_ij, Vector p0)
        {
            Ray[] rays = new Ray[scene.getTrans().Count() + 1];
            for (int t = 0; t < rays.Count() - 1; t++)
            {
                if (t == scene.getCam().Transformation)
                    rays[t] = null;
                else {
                    Transformation trans = transf[t];
                    Vector p0M = trans.changeSpace(p0, true, 1);
                    Vector dirM = trans.changeSpace(dir_ij, true, 0);
                    rays[t] = new Ray(p0M, dirM);
                }
            }
            rays[rays.Count() - 1] = new Ray(p0, dir_ij);
            return rays;
        }

        //Antigo "call"
        public void calculateRay()
        {
            int h_res = scene.getImag().Horizontal;
            int v_res = scene.getImag().Vertical;
            double fov = (scene.getCam().FieldOfView * Math.PI) / 180;
            double dist = scene.getCam().Distance;

            int totalPixels = h_res * v_res;
            int pixelCounter = 1;

            img = new Bitmap(h_res, v_res, PixelFormat.Format32bppRgb);
            //img = new BufferedImage(h_res, v_res, BufferedImage.TYPE_INT_RGB);

            double h = (Math.Tan(fov / 2) * 2 * dist);
            double w = ((h / v_res) * h_res);
            Vector p_cam = new Vector(0, 0, dist);

            prepareTransformation();

            for (int j = 0; j < v_res; j++)
            {
                double y_pix = -(j + 0.5) * (h / v_res) + (h / 2);
                for (int i = 0; i < h_res; i++)
                {
                    double x_pix = (i + 0.5) * (w / h_res) - (w / 2);
                    Vector p_pix = new Vector(x_pix, y_pix, 0);
                    Vector dir_ij = Vector.cVector(p_cam, p_pix);
                    dir_ij = dir_ij.normalize();

                    Ray[] rays = calcRaysTrasnf(dir_ij, p_cam);

                    Color pixelC = traceRay(rays, iter, false);


                    img.SetPixel(i, j, System.Drawing.Color.FromArgb(pixelC.getRGB()));
                    int c = pixelC.getRGB();
                    //updateProgress(pixelCounter, totalPixels);
                    pixelCounter++;

                }
            }
            //notify();
        }

        // Calculate the color of a certain pixel.
        private Color traceRay(Ray[] r, int rec, Boolean exiting)
        {

            Color pixelC = new Color(0, 0, 0);

            Intersection intersection = firstIntersection(r);

            if (intersection == null)
            {
                return scene.getImag().BackgroundColor;
            }
            else {

                Color matC = scene.getMats(intersection.getMat()).Color;
                Vector N = intersection.getNormal();
                Vector pInter = intersection.getpInter();
                Vector dirR = r[r.Count() - 1].Dir;

                double reflex = scene.getMats(intersection.getMat()).Reflection;
                double refracIndex = scene.getMats(intersection.getMat()).Index_refraction;
                double refraction = scene.getMats(intersection.getMat()).Refraction;
                double mab = scene.getMats(intersection.getMat()).Ambient;

                foreach (Light l in scene.getLights())
                {
                    Vector lightP = transf[l.Transformation]
                        .changeSpace(new Vector(0, 0, 0), false, 1);

                    if (amb)
                        pixelC.sumColor(Color.mulColor(Color.mulColor(matC, l.Color), mab));
                    else
                        pixelC.sumColor(matC);

                    Vector lightV = Vector.cVector(pInter, lightP).normalize();
                    Ray[] lightRs = calcRaysTrasnf(lightV, pInter);

                    if (shade)
                    {
                        if (!shadowIntersection(lightRs))
                        {
                            double shade = Vector.sProd(lightV, N);
                            if (shade > 0)
                            {
                                double shadeMdif = shade * scene.getMats(intersection.getMat()).Diffuse;
                                Color matClightC = Color.mulColor(l.Color, matC);
                                pixelC.sumColor(Color.mulColor(matClightC, shadeMdif));
                            }
                        }
                    }
                }

                if (rec > 0)
                {
                    double c1 = -(Vector.sProd(N, dirR));
                    if (reflex > 0 && reflect)
                    {
                        //double r0 = 0.91;

                        Vector rl = (dirR)
                                .sumVector(N.constVector(2 * c1))
                                .normalize();

                        Ray[] rays = calcRaysTrasnf(rl, pInter);
                        pixelC.sumColor(Color.mulColor(traceRay(rays, rec - 1, exiting), reflex));

                    }
                    if (refraction > 0 && refract)
                    {
                        double n = 1 / refracIndex;
                        if (exiting)
                        {
                            n = refracIndex / 1;

                        }
                        if (exiting) N = N.constVector(-1.0);
                        double c2 = Math.Sqrt(1 - n * n * (1 - c1 * c1));

                        Vector rR = (dirR.constVector(n)).sumVector(N.constVector(n * c1 - c2)).normalize();
                        Ray[] rays = calcRaysTrasnf(rR, pInter);

                        pixelC.sumColor(Color.mulColor(traceRay(rays, rec - 1, (n < 1)), refraction));

                    }
                }
            }
            return pixelC;
        }

        //Verifica se um certo raio intercepta o objecto
        private Boolean shadowIntersection(Ray[] rays)
        {
            Ray r;
            Vector p0 = rays[rays.Count() - 1].P0;
            Intersection intersection = null;
            Boolean intersect = false;
            foreach (Shape shape in scene.getShapes())
            {
                r = rays[shape.getTransformation()];
                if (shape.getShape() == 0)
                {
                    intersection = triangleIntersection(r, (Triangle)shape, p0);
                }
                if (shape.getShape() == 2)
                {
                    intersection = sphereIntersection(r, (Sphere)shape, p0);
                }
                if (shape.getShape() == 1)
                {
                    intersection = boxIntersection(new Ray(r.P0.constVector(1.0), r.Dir), (Box)shape, p0);
                }
                if (intersection != null)
                {
                    intersect = true;
                    break;
                }
            }
            return intersect;
        }

        //verify the closest object that a certain ray intercect
        private Intersection firstIntersection(Ray[] rays)
        {
            Ray r;
            Vector p0 = rays[rays.Count() - 1].P0;
            Double distClosest = Double.MaxValue;
            Intersection closest = null;
            Intersection currentInt = null;
            foreach (Shape shape in scene.getShapes())
            {
                r = rays[shape.getTransformation()];
                if (shape.getShape() == 0)
                {
                    currentInt = triangleIntersection(r, (Triangle)shape, p0);
                }
                if (shape.getShape() == 2)
                {
                    currentInt = sphereIntersection(r, (Sphere)shape, p0);
                }
                if (shape.getShape() == 1)
                {
                    currentInt = boxIntersection(r, (Box)shape, p0);
                }
                if (currentInt != null)
                {
                    if (currentInt.getDist() < distClosest)
                    {
                        distClosest = currentInt.getDist();
                        closest = currentInt;
                    }
                }
            }
            return closest;
        }

        
        //Verifies if the ray intersect a given circle, if so, returns a Intersection object.
        private Intersection sphereIntersection(Ray R, Sphere S, Vector p0)
        {


            Vector dirO = R.Dir;
            Vector p0O = R.P0;
            double a, b, c;

            a = Vector.sProd(dirO, dirO);
            b = 2 * dirO.getX() * p0O.getX() + 2 * dirO.getY() * p0O.getY() + 2 * dirO.getZ() * p0O.getZ();
            c = p0O.getX() * p0O.getX() + p0O.getY() * p0O.getY() + p0O.getZ() * p0O.getZ() - 1;

            if (b * b < 4 * a * c)
            {
                return null;
            }

            double d = Math.Sqrt(b * b - 4 * a * c);
            double tm, tp;

            tm = (-b - d) / (2 * a);
            tp = (-b + d) / (2 * a);

            if (tp < 0 || tm < 0)
            {
                return null;
            }

            double tmin;

            if (tp < tm)
                tmin = tp;
            else
                tmin = tm;

            Vector pInterO = p0O.sumVector(dirO.constVector(tmin + epsilon));
            Vector normalO = pInterO.normalize();

            Vector pInter = transf[S.getTransformation()].changeSpace(pInterO, false, 1);
            Vector normal = transf[S.getTransformation()].normalChangeSpace(normalO);

            Intersection inter = new Intersection(pInter, p0, S.getMaterial(), normal);

            return inter;

        }


        //Verifica se o raio intersecta o triangulo passado como argumento, se sim, retorna a instersecção do objecto.
        private Intersection triangleIntersection(Ray r, Triangle t, Vector p0)
        {
            Vector p0O = r.P0; // R
            Vector dirO = r.Dir;  // D

            Vector t_a = t.getV1(); // a
            Vector t_b = t.getV2(); // b
            Vector t_c = t.getV3(); // c

            double det_A = determinant3x3(t_a.getX() - t_b.getX(), t_a.getX() - t_c.getX(), dirO.getX(),
                    t_a.getY() - t_b.getY(), t_a.getY() - t_c.getY(), dirO.getY(),
                    t_a.getZ() - t_b.getZ(), t_a.getZ() - t_c.getZ(), dirO.getZ());
            double det_beta = determinant3x3(
                    t_a.getX() - p0O.getX(), t_a.getX() - t_c.getX(), dirO.getX(),
                    t_a.getY() - p0O.getY(), t_a.getY() - t_c.getY(), dirO.getY(),
                    t_a.getZ() - p0O.getZ(), t_a.getZ() - t_c.getZ(), dirO.getZ());
            double beta = det_beta / det_A;
            if (beta < 0)
            {
                return null;
            }
            double det_gama = determinant3x3(
                    t_a.getX() - t_b.getX(), t_a.getX() - p0O.getX(), dirO.getX(),
                    t_a.getY() - t_b.getY(), t_a.getY() - p0O.getY(), dirO.getY(),
                    t_a.getZ() - t_b.getZ(), t_a.getZ() - p0O.getZ(), dirO.getZ()
            );
            double gama = det_gama / det_A;
            if (gama < 0)
            {
                return null;
            }

            if (beta + gama >= 1)
            {
                return null;
            }
            double det_T = determinant3x3(
                    t_a.getX() - t_b.getX(), t_a.getX() - t_c.getX(), t_a.getX() - p0O.getX(),
                    t_a.getY() - t_b.getY(), t_a.getY() - t_c.getY(), t_a.getY() - p0O.getY(),
                    t_a.getZ() - t_b.getZ(), t_a.getZ() - t_c.getZ(), t_a.getZ() - p0O.getZ()
            );
            double T = det_T / det_A;

            if (T <= 10E-9)
            {
                return null;
            }

            Vector pInterO = p0O.sumVector(dirO.constVector(T + epsilon));
            Vector pInter = transf[t.getTransformation()].changeSpace(pInterO, false, 1);
            Vector normal = transf[t.getTransformation()].normalChangeSpace(t.getNormal());

            return new Intersection(pInter, p0, t.getMaterial(), normal);
        }

        //Verifica se o raio intercepta a caixa passada como argumento, se sim, retorna o objecto de interseção.
        private Intersection boxIntersection(Ray r, Box b, Vector p0)
        {
            Vector dirO = r.Dir;
            Vector p0O = r.P0;

            Vector normalO;

            double tmin = (-0.5 - p0O.getX()) / dirO.getX();
            double tmax = (0.5 - p0O.getX()) / dirO.getX();
            normalO = new Vector(-1, 0, 0);
            if (tmin > tmax)
            {
                double temp = tmin;
                tmin = tmax;
                tmax = temp;
                normalO = new Vector(1, 0, 0);
            }
            double tymin = (-0.5 - p0O.getY()) / dirO.getY();
            double tymax = (0.5 - p0O.getY()) / dirO.getY();
            double Ny = -1;
            if (tymin > tymax)
            {
                double temp = tymin;
                tymin = tymax;
                tymax = temp;
                Ny = 1;
            }
            if ((tmin > tymax) || (tymin > tmax))
            {
                return null;
            }
            if (tymin > tmin)
            {
                tmin = tymin;
                normalO = new Vector(0, Ny, 0);
            }
            if (tymax < tmax)
            {
                tmax = tymax;
            }
            double tzmin = (-0.5 - p0O.getZ()) / dirO.getZ();
            double tzmax = (0.5 - p0O.getZ()) / dirO.getZ();
            double Nz = -1;
            if (tzmin > tzmax)
            {
                double temp = tzmin;
                tzmin = tzmax;
                tzmax = temp;
                Nz = 1;
            }
            if ((tmin > tzmax) || (tzmin > tmax))
            {
                return null;
            }
            if (tzmin > tmin)
            {
                tmin = tzmin;
                normalO = new Vector(0, 0, Nz);
            }

            if (tmin < 0)
                return null;

            Vector pInterO = p0O.sumVector(dirO.constVector(tmin - epsilon));

            Vector pInter = transf[b.getTransformation()].changeSpace(pInterO, false, 1);
            Vector normal = transf[b.getTransformation()].normalChangeSpace(normalO);


            return new Intersection(pInter, p0, b.getMaterial(), normal);
        }

        public Bitmap getImage()
        {
            return img;
        }

    }
}
