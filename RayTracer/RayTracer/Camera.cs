﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //verified
    class Camera
    {
        private double distance, field_of_view;
        private int transformation;
       

        public Camera(int transformation, double dist, double field_of_view)
        {
            this.transformation = transformation;
            this.distance = dist;
            this.field_of_view = field_of_view;
        }

        public Camera()
        {
            this.transformation = 1;
            this.distance = 0;
            this.field_of_view = 0;
        }


        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public double Distance
        {
            get { return distance; }
            set {
                if (value >= 0.1)
                    distance = value;
                else distance = 0.1;
            }
        }

        public double FieldOfView
        {
            get { return field_of_view; }
            set {
                if (value >= 0.1)
                    field_of_view = value;
                else field_of_view = 0.1;
            }
        }
    }
}
