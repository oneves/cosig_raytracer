﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Light
    {

        Color color;
        private int transformation;

        public Light(int transformation, Color color)
        {
            this.color = color;
            this.transformation = transformation;
        }

        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }
    }
}
