﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Triangle : Shape
    {

        private Vector v1, v2, v3;
        private Vector normal;

        public Triangle(int transformation, int material, Vector v1, Vector v2, Vector v3) :
            base (transformation, material){
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            calcValues();
        }

        private void calcValues()
        {
            normal = Vector.vProd(Vector.cVector(v1, v2), Vector.cVector(v1, v3)).normalize();

        }

        public void changeSpace(Vector x, Vector y, Vector z)
        {
            v1 = x;
            v2 = y;
            v3 = z;
        }

        public Vector getV1()
        {
            return v1;
        }

        public Vector getV2()
        {
            return v2;
        }

        public Vector getV3()
        {
            return v3;
        }

        public Vector getNormal()
        {
            return normal;
        }


    }
}
