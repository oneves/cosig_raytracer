﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Vector
    {

        private double x, y, z;

        public Vector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double sProd(Vector v) {
            return this.x * v.x + this.y * v.y + this.z * v.z;

        }

        public static double sProd(Vector v1, Vector v2) {
            return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        }

        public Vector vProd(Vector v) {

            double x = this.y * v.z - this.z * v.y;
            double y = this.x * v.z - this.z * v.x;
            double z = this.x * v.y - this.y * v.x;

            return new Vector(x, y, z);

        }

        public static Vector vProd(Vector v1, Vector v2) {
            double x = v1.y * v2.z - v1.z * v2.y;
            double y = v1.x * v2.z - v1.z * v2.x;
            double z = v1.x * v2.y - v1.y * v2.x;


            return new Vector(x, y, z);
        }

        public static Vector cVector(Vector p1, Vector p2)
        {
            return new Vector(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        }

        public double nVector()
        {
            return (double)Math.Sqrt(x * x + y * y + z * z);
        }

        public Vector normalize()
        {
            double norm = this.nVector();
            double x = this.x / norm;
            double y = this.y / norm;
            double z = this.z / norm;

            return new Vector(x, y, z);
        }

        public Vector sumVector(Vector v)
        {
            return new Vector(this.x + v.x, this.y + v.y, z + v.z);
        }

        public Vector minVector(Vector v)
        {
            return new Vector(this.x - v.x, this.y - v.y, z - v.z);
        }

        public Vector constVector(Double c)
        {
            return new Vector(this.x * c, this.y * c, z * c);
        }

        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        public double getY()
        {
            return y;
        }

        public void setY(double y)
        {
            this.y = y;
        }

        public double getZ()
        {
            return z;
        }

        public void setZ(double z)
        {
            this.z = z;
        }
    }
}
