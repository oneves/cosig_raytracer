﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//verified
namespace RayTracer
{
    class Color
    {
        double r, g, b;

        public Color(double r, double g, double b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public void sumColor(Color c)
        {
            this.r += c.getColor()[0];
            this.g += c.getColor()[1];
            this.b += c.getColor()[2];
        }

        public static Color mulColor(Color c1, Color c2)
        {
            double r = c1.getColor()[0] * c2.getColor()[0];
            double g = c1.getColor()[1] * c2.getColor()[1];
            double b = c1.getColor()[2] * c2.getColor()[2];
            return new Color(r, g, b);
        }

        public static Color mulColor(Color c, double m)
        {
            return new Color(c.r * m, c.g * m, c.b * m);
        }

        public double[] getColor()
        {
            double[] color = { r, g, b };
            return color;
        }

        public int getRGB()
        {
            int ir, ig, ib;

            double auxR = r * 255;
            double auxG = g * 255;
            double auxB = b * 255;

            ir = Convert.ToInt32(auxR);
            ig = Convert.ToInt32(auxG);
            ib = Convert.ToInt32(auxB);

            return (ir << 16) | (ig << 8) | ib;
        }

        //GETTER and SETTER
        public double R
        {
            get { return r; }
            set
            {
                if (value < 0.0)
                {
                    r = 0.0;
                }
                else if (value > 1.0)
                {
                    r = 1.0;
                }
                else
                {
                    r = value;
                }
            }
        }

        public double G
        {
            get { return g; }
            set
            {
                if (value < 0.0)
                {
                    g = 0.0;
                }
                else if (value > 1.0)
                {
                    g = 1.0;
                }
                else
                {
                    g = value;
                }
            }
        }

        public double B
        {
            get { return b; }
            set
            {
                if (value < 0.0)
                {
                    b = 0.0;
                }
                else if (value > 1.0)
                {
                    b = 1.0;
                }
                else
                {
                    b = value;
                }
            }
        }
        
    }
}
