﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Material
    {
        Color color;
        double ambient, diffuse, reflection, refraction;
        double refracIndex;


        public Material(Color color, double ambient, double diffuse, double reflection, 
            double refraction, double refracIndex)
        {
            this.color = color;
            this.ambient = ambient;
            this.diffuse = diffuse;
            this.reflection = reflection;
            this.refraction = refraction;
            this.refracIndex = refracIndex;
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public double Index_refraction
        {
            get { return refracIndex; }
            set
            {
                if (value < 0.1)
                {
                    refracIndex = 0.1;
                }
                else
                {
                    refracIndex = value;
                }
            }
        }

        public double Refraction
        {
            get { return refraction; }
            set
            {
                if (value < 0.0)
                {
                    refraction = 0.0;
                }
                else if (value > 1.0)
                {
                    refraction = 1.0;
                }
                else
                {
                    refraction = value;
                }
            }
        }

        public double Reflection
        {
            get { return reflection; }
            set
            {
                if (value < 0.0)
                {
                    reflection = 0.0;
                }
                else if (value > 1.0)
                {
                    reflection = 1.0;
                }
                else
                {
                    reflection = value;
                }
            }
        }

        public double Diffuse
        {
            get { return diffuse; }
            set
            {
                if (value < 0.0)
                {
                    diffuse = 0.0;
                }
                else if (value > 1.0)
                {
                    diffuse = 1.0;
                }
                else
                {
                    diffuse = value;
                }
            }
        }

        public double Ambient
        {
            get { return ambient; }
            set
            {
                if (value < 0.0)
                {
                    ambient = 0.0;
                }
                else if (value > 1.0)
                {
                    ambient = 1.0;
                }
                else
                {
                    ambient = value;
                }
            }
        }
    }
}
