﻿using DotNetMatrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Transformation
    {
        //Translation
        //double tx, ty, tz;
        //Rotation
        //double rx, ry, rz;
        //Scale
        //double sx, sy, sz;

        double[,] transformMatrix2 = new double[4, 4];

        private double[] translation;
        private double[] rotation;

        private GeneralMatrix transformMatrix;
        private GeneralMatrix tranformInv;


        public Transformation()
        {
            transformMatrix = GeneralMatrix.Identity(4, 4);
            translation = new double[3];
            rotation = new double[3];
        }

        public Transformation(Transformation t)
        {
            this.transformMatrix = t.transformMatrix;
            this.tranformInv = t.tranformInv;
            this.rotation = t.rotation;
            this.translation = t.translation;
        }

        public void identityMatrix()
        {
            transformMatrix = GeneralMatrix.Identity(4, 4);

        }

        public void translate(double x, double y, double z)
        {

            //GeneralMatrix translateMatrix = new GeneralMatrix(4, 4);
            //double[,] translateMatrix = new double[4,4];
            double[][] translateMatrix = new double[4][];

            translateMatrix[0] = new double[4];
            translateMatrix[1] = new double[4];
            translateMatrix[2] = new double[4];
            translateMatrix[3] = new double[4];

            translation[0] = x;
            translation[1] = y;
            translation[2] = z;

            translateMatrix[0][0] = 1.0;
            translateMatrix[0][1] = 0.0;
            translateMatrix[0][2] = 0.0;
            translateMatrix[0][3] = x;

            translateMatrix[1][0] = 0.0;
            translateMatrix[1][1] = 1.0;
            translateMatrix[1][2] = 0.0;
            translateMatrix[1][3] = y;

            translateMatrix[2][0] = 0.0;
            translateMatrix[2][1] = 0.0;
            translateMatrix[2][2] = 1.0;
            translateMatrix[2][3] = z;

            translateMatrix[3][0] = 0.0;
            translateMatrix[3][1] = 0.0;
            translateMatrix[3][2] = 0.0;
            translateMatrix[3][3] = 1.0;

            /*
            translateMatrix.Array[0][0] = 1.0;
            translateMatrix.Array[0][1] = 0.0;
            translateMatrix.Array[0][2] = 0.0;
            translateMatrix.Array[0][3] = x;

            translateMatrix.Array[1][0] = 0.0;
            translateMatrix.Array[1][1] = 1.0;
            translateMatrix.Array[1][2] = 0.0;
            translateMatrix.Array[1][3] = y;

            translateMatrix.Array[2][0] = 0.0;
            translateMatrix.Array[2][1] = 0.0;
            translateMatrix.Array[2][2] = 1.0;
            translateMatrix.Array[2][3] = z;

            translateMatrix.Array[3][0] = 0.0;
            translateMatrix.Array[3][1] = 0.0;
            translateMatrix.Array[3][2] = 0.0;
            translateMatrix.Array[3][3] = 1.0;*/

            //transformMatrix = transformMatrix.Multiply(translateMatrix);
            transformMatrix = transformMatrix.Multiply(GeneralMatrix.Create(translateMatrix));

        }

        public void rotateX(double a)
        {

            //GeneralMatrix rotateXMatrix = new GeneralMatrix(4, 4);

            double[][] rotateXMatrix = new double[4][];
            rotateXMatrix[0] = new double[4];
            rotateXMatrix[1] = new double[4];
            rotateXMatrix[2] = new double[4];
            rotateXMatrix[3] = new double[4];

            rotation[0] = a;

            a *= Math.PI / 180.0;

            rotateXMatrix[0][0] = 1.0;
            rotateXMatrix[0][1] = 0.0;
            rotateXMatrix[0][2] = 0.0;
            rotateXMatrix[0][3] = 0.0;

            rotateXMatrix[1][0] = 0.0;
            rotateXMatrix[1][1] = Math.Cos(a);
            rotateXMatrix[1][2] = Math.Sin(a);
            rotateXMatrix[1][3] = 0.0;

            rotateXMatrix[2][0] = 0.0;
            rotateXMatrix[2][1] = -Math.Sin(a);
            rotateXMatrix[2][2] = Math.Cos(a);
            rotateXMatrix[2][3] = 0.0;

            rotateXMatrix[3][0] = 0.0;
            rotateXMatrix[3][1] = 0.0;
            rotateXMatrix[3][2] = 0.0;
            rotateXMatrix[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(GeneralMatrix.Create(rotateXMatrix));
            /*
            rotateXMatrix.Array[0][0] = 1.0;
            rotateXMatrix.Array[0][1] = 0.0;
            rotateXMatrix.Array[0][2] = 0.0;
            rotateXMatrix.Array[0][3] = 0.0;

            rotateXMatrix.Array[1][0] = 0.0;
            rotateXMatrix.Array[1][1] = Math.Cos(a);
            rotateXMatrix.Array[1][2] = Math.Sin(a);
            rotateXMatrix.Array[1][3] = 0.0;

            rotateXMatrix.Array[2][0] = 0.0;
            rotateXMatrix.Array[2][1] = - Math.Sin(a);
            rotateXMatrix.Array[2][2] = Math.Cos(a);
            rotateXMatrix.Array[2][3] = 0.0;

            rotateXMatrix.Array[3][0] = 0.0;
            rotateXMatrix.Array[3][1] = 0.0;
            rotateXMatrix.Array[3][2] = 0.0;
            rotateXMatrix.Array[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(rotateXMatrix);*/



        }

        public void rotateY(double a)
        {
            //GeneralMatrix rotateYMatrix = new GeneralMatrix(4, 4);
            double[][] rotateYMatrix = new double[4][];
            rotateYMatrix[0] = new double[4];
            rotateYMatrix[1] = new double[4];
            rotateYMatrix[2] = new double[4];
            rotateYMatrix[3] = new double[4];

            rotation[1] = a;

            a *= Math.PI / 180.0;

            rotateYMatrix[0][0] = Math.Cos(a);
            rotateYMatrix[0][1] = 0.0;
            rotateYMatrix[0][2] = Math.Sin(a);
            rotateYMatrix[0][3] = 0.0;

            rotateYMatrix[1][0] = 0.0;
            rotateYMatrix[1][1] = 1.0;
            rotateYMatrix[1][2] = 0.0;
            rotateYMatrix[1][3] = 0.0;

            rotateYMatrix[2][0] = -Math.Sin(a);
            rotateYMatrix[2][1] = 0.0;
            rotateYMatrix[2][2] = Math.Cos(a);
            rotateYMatrix[2][3] = 0.0;

            rotateYMatrix[3][0] = 0.0;
            rotateYMatrix[3][1] = 0.0;
            rotateYMatrix[3][2] = 0.0;
            rotateYMatrix[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(GeneralMatrix.Create(rotateYMatrix));

            /*
            rotateYMatrix.Array[0][0] = Math.Cos(a);
            rotateYMatrix.Array[0][1] = 0.0;
            rotateYMatrix.Array[0][2] = Math.Sin(a);
            rotateYMatrix.Array[0][3] = 0.0;

            rotateYMatrix.Array[1][0] = 0.0;
            rotateYMatrix.Array[1][1] = 1.0;
            rotateYMatrix.Array[1][2] = 0.0;
            rotateYMatrix.Array[1][3] = 0.0;

            rotateYMatrix.Array[2][0] = -Math.Sin(a);
            rotateYMatrix.Array[2][1] = 0.0;
            rotateYMatrix.Array[2][2] = Math.Cos(a);
            rotateYMatrix.Array[2][3] = 0.0;

            rotateYMatrix.Array[3][0] = 0.0;
            rotateYMatrix.Array[3][1] = 0.0;
            rotateYMatrix.Array[3][2] = 0.0;
            rotateYMatrix.Array[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(rotateYMatrix);*/

        }

        public void rotateZ(double a)
        {
            //GeneralMatrix rotateZMatrix = new GeneralMatrix(4, 4);
            double[][] rotateZMatrix = new double[4][];
            rotateZMatrix[0] = new double[4];
            rotateZMatrix[1] = new double[4];
            rotateZMatrix[2] = new double[4];
            rotateZMatrix[3] = new double[4];

            rotation[2] = a;

            a *= Math.PI / 180.0;

            rotateZMatrix[0][0] = Math.Cos(a);
            rotateZMatrix[0][1] = -Math.Sin(a);
            rotateZMatrix[0][2] = 0.0;
            rotateZMatrix[0][3] = 0.0;

            rotateZMatrix[1][0] = Math.Sin(a);
            rotateZMatrix[1][1] = Math.Cos(a);
            rotateZMatrix[1][2] = 0.0;
            rotateZMatrix[1][3] = 0.0;

            rotateZMatrix[2][0] = 0.0;
            rotateZMatrix[2][1] = 0.0;
            rotateZMatrix[2][2] = 1.0;
            rotateZMatrix[2][3] = 0.0;

            rotateZMatrix[3][0] = 0.0;
            rotateZMatrix[3][1] = 0.0;
            rotateZMatrix[3][2] = 0.0;
            rotateZMatrix[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(GeneralMatrix.Create(rotateZMatrix));

            /*
            rotateZMatrix.Array[0][0] = Math.Cos(a);
            rotateZMatrix.Array[0][1] = -Math.Sin(a);
            rotateZMatrix.Array[0][2] = 0.0;
            rotateZMatrix.Array[0][3] = 0.0;

            rotateZMatrix.Array[1][0] = Math.Sin(a);
            rotateZMatrix.Array[1][1] = Math.Cos(a);
            rotateZMatrix.Array[1][2] = 0.0;
            rotateZMatrix.Array[1][3] = 0.0;

            rotateZMatrix.Array[2][0] = 0.0;
            rotateZMatrix.Array[2][1] = 0.0;
            rotateZMatrix.Array[2][2] = 1.0;
            rotateZMatrix.Array[2][3] = 0.0;

            rotateZMatrix.Array[3][0] = 0.0;
            rotateZMatrix.Array[3][1] = 0.0;
            rotateZMatrix.Array[3][2] = 0.0;
            rotateZMatrix.Array[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(rotateZMatrix);*/

        }


        public void scale(double x, double y, double z)
        {

            //GeneralMatrix scaleMatrix = new GeneralMatrix(4, 4);
            double[][] scaleMatrix = new double[4][];
            scaleMatrix[0] = new double[4];
            scaleMatrix[1] = new double[4];
            scaleMatrix[2] = new double[4];
            scaleMatrix[3] = new double[4];

            scaleMatrix[0][0] = x;
            scaleMatrix[0][1] = 0.0;
            scaleMatrix[0][2] = 0.0;
            scaleMatrix[0][3] = 0.0;

            scaleMatrix[1][0] = 0.0;
            scaleMatrix[1][1] = y;
            scaleMatrix[1][2] = 0.0;
            scaleMatrix[1][3] = 0.0;

            scaleMatrix[2][0] = 0.0;
            scaleMatrix[2][1] = 0.0;
            scaleMatrix[2][2] = z;
            scaleMatrix[2][3] = 0.0;

            scaleMatrix[3][0] = 0.0;
            scaleMatrix[3][1] = 0.0;
            scaleMatrix[3][2] = 0.0;
            scaleMatrix[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(GeneralMatrix.Create(scaleMatrix));

            /*
            scaleMatrix.Array[0][0] = x;
            scaleMatrix.Array[0][1] = 0.0;
            scaleMatrix.Array[0][2] = 0.0;
            scaleMatrix.Array[0][3] = 0.0;

            scaleMatrix.Array[1][0] = 0.0;
            scaleMatrix.Array[1][1] = y;
            scaleMatrix.Array[1][2] = 0.0;
            scaleMatrix.Array[1][3] = 0.0;

            scaleMatrix.Array[2][0] = 0.0;
            scaleMatrix.Array[2][1] = 0.0;
            scaleMatrix.Array[2][2] = z;
            scaleMatrix.Array[2][3] = 0.0;

            scaleMatrix.Array[3][0] = 0.0;
            scaleMatrix.Array[3][1] = 0.0;
            scaleMatrix.Array[3][2] = 0.0;
            scaleMatrix.Array[3][3] = 1.0;

            transformMatrix = transformMatrix.Multiply(scaleMatrix);*/
        }

        public void invertMatrix() {
            tranformInv = transformMatrix.Inverse();
        }

        public Vector changeSpace(Vector v, Boolean invert, int w)
        {
            double[][] mC = { 
                new double [] { v.getX() },
                new double [] { v.getY() },
                new double [] { v.getZ() },
                new double [] { w }
            };

            GeneralMatrix matrixC;

            if (invert)
                matrixC = tranformInv.Multiply(GeneralMatrix.Create(mC));
            else
                matrixC = transformMatrix.Multiply(GeneralMatrix.Create(mC));

            double x = matrixC.GetElement(0, 0);
            double y = matrixC.GetElement(1, 0);
            double z = matrixC.GetElement(2, 0);
            double wr = matrixC.GetElement(3, 0);

            Vector returnV;

            if (w == 0)
                returnV = new Vector(x, y, z).normalize();
            else
                returnV = new Vector(x / wr, y / wr, z / wr);

            return returnV;
        }

        public Vector normalChangeSpace(Vector v)
        {

            double[][] mC = {
                new double [] { v.getX() },
                new double [] { v.getY() },
                new double [] { v.getZ() },
                new double [] { 0 }
            };

            GeneralMatrix matrixCt = tranformInv.Transpose();
            GeneralMatrix matrixC = matrixCt.Multiply(GeneralMatrix.Create(mC));

            double x = matrixC.GetElement(0, 0);
            double y = matrixC.GetElement(1, 0);
            double z = matrixC.GetElement(2, 0);

            return new Vector(x, y, z).normalize();
        }

        public void mult(Transformation t)
        {
            transformMatrix = (t.tranformInv).Multiply(transformMatrix);

            //multiply3(transformMatrix.Array);
            tranformInv = transformMatrix.Inverse();
        }

        public double[] getTranslate()
        {
            return translation;
        }

        public double[] getRotation()
        {
            return rotation;
        }

        //NOVAS CLASSES DE OPERAÇÃO

        void multiply3(double[][] matrixA)
        {
            int i, j, k;
            double[][] matrixB = new double[4] [];
            matrixB[0] = new double[4];
            matrixB[1] = new double[4];
            matrixB[2] = new double[4];
            matrixB[3] = new double[4];

            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                {
                    matrixB[i] [j] = transformMatrix.Array[i] [j];
                    transformMatrix.Array[i] [j] = 0.0;
                }
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    for (k = 0; k < 4; k++)
                        transformMatrix.Array[i] [j] += matrixA[i] [k] * matrixB[k] [j];
        }



    }
}

