﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Ray
    {
        private Vector p0, dir;

        public Ray(Vector p0, Vector dir)
        {
            this.p0 = p0;
            this.dir = dir;
        }

        public Vector P0
        {
            get { return p0; }
            set { p0 = value; }
        }

        public Vector Dir
        {
            get { return dir; }
            set { dir = value; }
        }
    }
}
