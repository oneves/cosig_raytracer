﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Scene
    {
        private Camera cam;
        private Image imag;
        private List<Light> lights;
        private List<Transformation> trans;
        private List<Material> mats;
        private List<Shape> shapes;

        public Scene()
        {
            this.trans = new List<Transformation>();
            this.mats = new List<Material>();
            this.shapes = new List<Shape>();
            this.lights = new List<Light>();
        }

        public Scene(Camera camera, Image image, List<Light> lights, 
            List<Transformation> trans, List<Material> mats, List<Shape> shapes)
        {
            this.cam = camera;
            this.imag = image;
            this.trans = trans;
            this.mats = mats;
            this.shapes = shapes;
            this.lights = lights;
        }

        public Camera getCam()
        {
            return cam;
        }
        public Image getImag()
        {
            return imag;
        }
        public List<Transformation> getTrans()
        {
            return trans;
        }
        public Material getMats(int m)
        {
            return mats.ElementAt(m);
        }
        public List<Shape> getShapes()
        {
            return shapes;
        }
        public List<Light> getLights()
        {
            return lights;
        }

        public void addTransformation(Transformation T)
        {
            trans.Insert(trans.Count, T);
        }
        public void editTransformation(Transformation T, int pos)
        {
            trans[pos] = T;
        }
        public void addMaterial(Material M)
        {
            mats.Insert(mats.Count, M);
        }
        public void addShape(Shape S)
        {
            shapes.Insert(shapes.Count, S);
        }
        public void addLight(Light L)
        {
            lights.Insert(lights.Count, L);
        }
        public void setCam(Camera cam)
        {
            this.cam = cam;
        }
        public void setImag(Image imag)
        {
            this.imag = imag;
        }
    }
}
