﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace RayTracer
{
    //Verified
    class Image
    {
        private int horizontal, vertical;
        private Color color;

        public Image(Color backgroundcolor, int h_res, int v_res)
        {
            this.horizontal = h_res;
            this.vertical = v_res;
            this.color = backgroundcolor;
        }

        public int Horizontal
        {
            get { return this.horizontal; }
            set { this.horizontal = value; }
        }

        public int Vertical
        {
            get { return this.vertical; }
            set { this.vertical = value; }
        }

        public Color BackgroundColor
        {
            get { return this.color; }
            set { this.color = value; }
        }

    }
}
