﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    //Verified
    class Shape
    {

        protected int material;
        protected int transformation;
        //0-Triangle, 1-Box, 2-Sphere
        protected char shape;

        public Shape(int t, int m)
        {
            this.material = m;
            this.transformation = t;
        }

        public int getMaterial()
        {
            return material;
        }

        public int getTransformation()
        {
            return transformation;
        }

        public char getShape()
        {
            return shape;
        }
    }
}
