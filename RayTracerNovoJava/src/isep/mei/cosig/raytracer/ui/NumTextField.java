package isep.mei.cosig.raytracer.ui;

import javafx.scene.control.TextField;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class NumTextField extends TextField {

    @Override
    public void replaceText(int i, int i1, String string){
        if (string.matches("-?([0-9]*(\\.?[0-9]*)?)?") || string.isEmpty()){
            super.replaceText(i, i1, string);
        }
    }

    @Override
    public void replaceSelection(String string){
        super.replaceSelection(string);
    }
}
