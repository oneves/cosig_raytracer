package isep.mei.cosig.raytracer.ui;

import isep.mei.cosig.raytracer.objects.Scene;
import isep.mei.cosig.raytracer.objects.*;
import isep.mei.cosig.raytracer.utils.RayTracer;
import isep.mei.cosig.raytracer.utils.ReadSceneFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class RayTraceFrameController implements Initializable {

    private Scene scene;
    private BufferedImage img;
    private File sceneFile;
    private ObservableList<Integer> cbOptions;


    @FXML
    Button exportMI;
    @FXML Button reloadMI;
    @FXML
    Button renderBT;
    @FXML
    ImageView prevImg;
    @FXML ImageView rootImg;
    @FXML
    CheckBox cameraEE;
    @FXML
    TextField cpX;
    @FXML TextField cpY;
    @FXML TextField cpZ;
    @FXML TextField crX;
    @FXML TextField crY;
    @FXML TextField crZ;
    @FXML TextField cFOV;
    @FXML TextField cDist;
    @FXML CheckBox lightEE;
    @FXML TextField lpX;
    @FXML TextField lpY;
    @FXML TextField lpZ;
    @FXML ComboBox<Integer> lightCB;
    @FXML ColorPicker colorPalet;
    @FXML CheckBox renderEE;
    @FXML TextField rX;
    @FXML TextField rY;
    @FXML TextField rIt;
    @FXML CheckBox refracCB;
    @FXML CheckBox reflexCB;
    @FXML CheckBox shadeCB;
    @FXML CheckBox ambCB;
    @FXML CheckBox ctoCB;
    @FXML ProgressBar progBar;
    @FXML Button loadBtn;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cbOptions= FXCollections.observableList(new ArrayList());
    }

    @FXML
    private void openMBHandler(){
        FileChooser fileChooser = new FileChooser();
        sceneFile = fileChooser.showOpenDialog(loadBtn.getScene().getWindow());
        if (sceneFile!=null){
            try{
                prevImg.setImage(null);
                loadScene(sceneFile,false);
                exportMI.setDisable(true);
                renderBT.setDisable(false);
                reloadMI.setDisable(false);
                loadBtn.setDisable(true);
            }catch(RuntimeException ex){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error:");
                alert.setContentText("Cannot read scene file!");
                alert.showAndWait();
            }

        }
    }

    @FXML
    private void exportMBHandler(){
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(exportMI.getScene().getWindow());
        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.jpeg");
        fileChooser.getExtensionFilters().add(extFilter);
        saveImg(img, file, "png");

    }

    @FXML
    private void reloadMBHandler(){
        loadScene(sceneFile,true);
    }

    @FXML
    private void renderBTHandler() throws InterruptedException{
        int iter=9;
        boolean amb=true;
        boolean sha=true;
        boolean refle=true;
        boolean refra=true;
        if(cameraEE.isSelected()){
            Transformation trans= new Transformation();
            if (ctoCB.isSelected()){
                trans.rotateZ(-Double.parseDouble(crZ.getText()));
                trans.rotateX(-Double.parseDouble(crX.getText()));
                trans.rotateY(-Double.parseDouble(crY.getText()));
                trans.translate(-Double.parseDouble(cpX.getText()),-Double.parseDouble(cpY.getText()),-Double.parseDouble(cpZ.getText()));

            }
            else{
                trans.rotateZ(Double.parseDouble(crZ.getText()));
                trans.rotateX(Double.parseDouble(crX.getText()));
                trans.rotateY(Double.parseDouble(crY.getText()));
                trans.translate(Double.parseDouble(cpX.getText()),Double.parseDouble(cpY.getText()),Double.parseDouble(cpZ.getText()));
            }
            trans.invertMatrix();
            scene.editTransformation(trans, scene.getCam().getTransformation());
            scene.getCam().setDist(Float.parseFloat(cDist.getText()));
            scene.getCam().setField_of_view(Float.parseFloat(cFOV.getText()));
        }
        if(lightEE.isSelected()){
            Transformation trans= new Transformation();
            System.out.println(lpX.getText()+" "+lpY.getText()+" "+lpZ.getText());
            trans.translate(Double.parseDouble(lpX.getText()),
                    Double.parseDouble(lpY.getText()),
                    Double.parseDouble(lpZ.getText()));
            trans.invertMatrix();
             scene.editTransformation(trans, scene.getLights().get(lightCB.getValue()-1).getTransformation());
            Color lColor=new Color(colorPalet.getValue().getRed(),colorPalet.getValue().getGreen(),colorPalet.getValue().getBlue());
            scene.getLights().get(lightCB.getValue()-1).setColor(lColor);
        }
        if(renderEE.isSelected()){
            System.out.println(rIt.getText());
            iter=Integer.parseInt(rIt.getText());
            amb=ambCB.isSelected();
            sha=shadeCB.isSelected();
            refle=reflexCB.isSelected();
            refra=refracCB.isSelected();
            scene.getImag().setRes(Integer.parseInt(rX.getText()), Integer.parseInt(rY.getText()));
        }
        RayTracer rayt=new RayTracer(scene, iter, amb, sha, refle, refra);
        progBar.progressProperty().unbind();
        progBar.setProgress(0);
        progBar.progressProperty().bind(rayt.progressProperty());
        Thread task=new Thread(rayt);
        task.start();
        rayt.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            public void handle(WorkerStateEvent t) {
                img=rayt.getImage();
                Image image = SwingFXUtils.toFXImage(img, null);
                prevImg.setImage(image);
                exportMI.setDisable(false);

            }
        });
    }

    @FXML
    private void cEditionCB(){
        if (cameraEE.isSelected()){
            cpX.setDisable(false);
            cpY.setDisable(false);
            cpZ.setDisable(false);
            crX.setDisable(false);
            crY.setDisable(false);
            crZ.setDisable(false);
            cFOV.setDisable(false);
            cDist.setDisable(false);
            ctoCB.setDisable(false);
        }
        else{
            cpX.setDisable(true);
            cpY.setDisable(true);
            cpZ.setDisable(true);
            crX.setDisable(true);
            crY.setDisable(true);
            crZ.setDisable(true);
            cFOV.setDisable(true);
            cDist.setDisable(true);
            ctoCB.setDisable(true);
        }
    }

    @FXML
    private void lEditionCB(){
        if (lightEE.isSelected()){
            lpX.setDisable(false);
            lpY.setDisable(false);
            lpZ.setDisable(false);
            lightCB.setDisable(false);
            colorPalet.setDisable(false);
        }
        else{
            lpX.setDisable(true);
            lpY.setDisable(true);
            lpZ.setDisable(true);
            lightCB.setDisable(true);
            colorPalet.setDisable(true);
        }
    }

    @FXML
    private void rEditionCB(){
        if (renderEE.isSelected()){
            rX.setDisable(false);
            rY.setDisable(false);
            rIt.setDisable(false);
            refracCB.setDisable(false);
            reflexCB.setDisable(false);
            shadeCB.setDisable(false);
            ambCB.setDisable(false);
        }
        else{
            rX.setDisable(true);
            rY.setDisable(true);
            rIt.setDisable(true);
            refracCB.setDisable(true);
            reflexCB.setDisable(true);
            shadeCB.setDisable(true);
            ambCB.setDisable(true);
        }
    }

    @FXML
    private void lightCBHandler(){
        lpX.setText(Double.toString(scene.getTrans().get(scene.getLights().get(lightCB.getValue()-1).getTransformation()).getTranslate()[0]));
        lpY.setText(Double.toString(scene.getTrans().get(scene.getLights().get(lightCB.getValue()-1).getTransformation()).getTranslate()[1]));
        lpZ.setText(Double.toString(scene.getTrans().get(scene.getLights().get(lightCB.getValue()-1).getTransformation()).getTranslate()[2]));
    }


    private void loadScene(File file, boolean reload){
        try {
            // TODO code application logic here
            ReadSceneFile read = new ReadSceneFile();
            scene = read.readSceneFromFile(file);
            setValues();
            Transformation trans= new Transformation();
            if (ctoCB.isSelected()){
                trans.rotateZ(-Double.parseDouble(crZ.getText()));
                trans.rotateX(-Double.parseDouble(crX.getText()));
                trans.rotateY(-Double.parseDouble(crY.getText()));
                trans.translate(-Double.parseDouble(cpX.getText()),-Double.parseDouble(cpY.getText()),-Double.parseDouble(cpZ.getText()));

            }
            else{
                trans.rotateZ(Double.parseDouble(crZ.getText()));
                trans.rotateX(Double.parseDouble(crX.getText()));
                trans.rotateY(Double.parseDouble(crY.getText()));
                trans.translate(Double.parseDouble(cpX.getText()),Double.parseDouble(cpY.getText()),Double.parseDouble(cpZ.getText()));
            }
            trans.invertMatrix();
            scene.editTransformation(trans, scene.getCam().getTransformation());
            System.out.println("Loading complete!");
            if (!reload){
                RayTracer rayt=new RayTracer(scene, 9, false, false, false, false);
                Thread task=new Thread(rayt);
                task.start();

                synchronized(task){
                    task.wait();
                    img=rayt.getImage();
                    Image image = SwingFXUtils.toFXImage(img, null);
                    //rootImg.setImage(image);
                    prevImg.setImage(image);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(RayTracer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(RayTraceFrameController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void saveImg(BufferedImage img, File file, String ext){
        try {
            ImageIO.write(img, ext, file);
        } catch (IOException ex) {
            Logger.getLogger(RayTracer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setValues(){
        cpX.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getTranslate()[0]));
        cpY.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getTranslate()[1]));
        cpZ.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getTranslate()[2]));
        crX.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getRotation()[0]));
        crY.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getRotation()[1]));
        crZ.setText(Double.toString(scene.getTrans().get(scene.getCam().getTransformation()).getRotation()[2]));
        cFOV.setText(Float.toString(scene.getCam().getFOV()));
        cDist.setText(Float.toString(scene.getCam().getDist()));


        lpX.setText(Double.toString(scene.getTrans().get(scene.getLights().get(0).getTransformation()).getTranslate()[0]));
        lpY.setText(Double.toString(scene.getTrans().get(scene.getLights().get(0).getTransformation()).getTranslate()[1]));
        lpZ.setText(Double.toString(scene.getTrans().get(scene.getLights().get(0).getTransformation()).getTranslate()[2]));
        for (int i=0; i<scene.getLights().size();i++){
            cbOptions.add(i+1);
        }
        lightCB.setItems(cbOptions);
        lightCB.setValue(1);
//            colorPalet.setDisable(false);
        rX.setText(Integer.toString(scene.getImag().getHRes()));
        rY.setText(Integer.toString(scene.getImag().getVRes()));
        rIt.setText("9");
        refracCB.setDisable(true);
        reflexCB.setDisable(true);
        shadeCB.setDisable(true);
        ambCB.setDisable(true);
    }
}
