package isep.mei.cosig.raytracer.utils;

import isep.mei.cosig.raytracer.objects.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class ReadSceneFile {

    private Scene scene;
    private String header;
    private boolean readingHeader=true;
    final static Charset ENCODING = StandardCharsets.UTF_8;

    public ReadSceneFile() {
        scene=new Scene();
    }



    public Scene readSceneFromFile(File file) throws IOException {

        String s;
        try (Scanner scanner =  new Scanner(file, ENCODING.name())){
            while (scanner.hasNextLine()){
                //process each line in some way
                s=scanner.nextLine();
                switch (s) {
                    case "\n":
                        continue;
                    case "Image":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] size=new String[2];
                            String[] color=new String[3];
                            size=s.split("\t");
                            size=size[1].split(" ");

                            s=scanner.nextLine();
                            color=s.split("\t");
                            color=color[1].split(" ");

                            s=scanner.nextLine();

                            Color c = new Color(Float.parseFloat(color[0]),
                                    Float.parseFloat(color[1]),
                                    Float.parseFloat(color[2]));
                            scene.setImag(new Image(Integer.parseInt(size[0]),
                                    Integer.parseInt(size[1]), c));
                        }   break;

                    case "Transformation":
                        scanner.nextLine();
                        Transformation transformation=new Transformation();
                        s=scanner.nextLine();
                        String[] transf=new String[4];
                        while (!s.equals("}")){
                            transf=s.split("\t");
                            transf=transf[1].split(" ");
                            switch (transf[0]){
                                case "I":
                                    break;
                                case "T":
                                    transformation.translate(Double.parseDouble(transf[1]),
                                            Double.parseDouble(transf[2]),
                                            Double.parseDouble(transf[3]));
                                    break;
                                case "Rx":
                                    transformation.rotateX(Double.parseDouble(transf[1]));
                                    break;
                                case "Ry":
                                    transformation.rotateY(Double.parseDouble(transf[1]));
                                    break;
                                case "Rz":
                                    transformation.rotateZ(Double.parseDouble(transf[1]));
                                    break;
                                case "S":
                                    transformation.scale(Double.parseDouble(transf[1]),
                                            Double.parseDouble(transf[2]),
                                            Double.parseDouble(transf[3]));
                                    break;
                            }
                            s=scanner.nextLine();
                        }
                        transformation.invertMatrix();
                        scene.addTransformation(transformation);
                        break;
                    case "Material":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] prop=new String[5];
                            String[] color=new String[3];
                            color=s.split("\t");
                            color=color[1].split(" ");
                            Color c = new Color(Float.parseFloat(color[0]),
                                    Float.parseFloat(color[1]),
                                    Float.parseFloat(color[2]));

                            s=scanner.nextLine();
                            prop=s.split("\t");
                            prop=prop[1].split(" ");

                            scene.addMaterial(new Material(c,
                                    Float.parseFloat(prop[0]),
                                    Float.parseFloat(prop[1]),
                                    Float.parseFloat(prop[2]),
                                    Float.parseFloat(prop[3]),
                                    Float.parseFloat(prop[4])));
                            s=scanner.nextLine();
                        }   break;
                    case "Camera":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        String[] prop=new String[3];
                        int count=0;
                        while (!s.equals("}")){
                            String[] val=new String[2];
                            val=s.split("\t");
                            prop[count]=val[1];
                            s=scanner.nextLine();
                            count++;
                        }
                        scene.setCam(new Camera(Integer.parseInt(prop[0]),
                                Float.parseFloat(prop[1]),
                                Float.parseFloat(prop[2])));
                        break;
                    case "Light":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] trans=new String[2];
                            String[] color=new String[3];
                            trans=s.split("\t");

                            s=scanner.nextLine();
                            color=s.split("\t");
                            color=color[1].split(" ");
                            Color c = new Color(Float.parseFloat(color[0]),
                                    Float.parseFloat(color[1]),
                                    Float.parseFloat(color[2]));

                            scene.addLight(new Light(Integer.parseInt(trans[1]), c));

                            s=scanner.nextLine();
                        }   break;
                    case "Sphere":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] trans=new String[2];
                            String[] mat=new String[2];
                            trans=s.split("\t");

                            s=scanner.nextLine();
                            mat=s.split("\t");

                            scene.addShape(new Sphere(Integer.parseInt(trans[1]),
                                    Integer.parseInt(mat[1])));
                            s=scanner.nextLine();
                        }   break;
                    case "Box":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] trans=new String[2];
                            trans=s.split("\t");
                            s=scanner.nextLine();

                            String[] mat=new String[2];
                            mat=s.split("\t");
                            scene.addShape(new Box(Integer.parseInt(trans[1]),
                                    Integer.parseInt(mat[1])));
                            s=scanner.nextLine();
                        }   break;
                    case "Triangles":
                        scanner.nextLine();
                        s=scanner.nextLine();
                        String[] trans=new String[2];
                        trans=s.split("\t");
                        s=scanner.nextLine();
                        while (!s.equals("}")){
                            String[] mat=new String[2];
                            mat=s.split("\t");
                            s=scanner.nextLine();

                            String[] point1=new String[3];
                            point1=s.split("\t");
                            point1=point1[1].split(" ");
                            Vector p1=new Vector(Float.parseFloat(point1[0]),
                                    Float.parseFloat(point1[1]),
                                    Float.parseFloat(point1[2]));
                            s=scanner.nextLine();

                            String[] point2=new String[3];
                            point2=s.split("\t");
                            point2=point2[1].split(" ");
                            s=scanner.nextLine();
                            Vector p2=new Vector(Float.parseFloat(point2[0]),
                                    Float.parseFloat(point2[1]),
                                    Float.parseFloat(point2[2]));

                            String[] point3=new String[3];
                            point3=s.split("\t");
                            point3=point3[1].split(" ");
                            Vector p3=new Vector(Float.parseFloat(point3[0]),
                                    Float.parseFloat(point3[1]),
                                    Float.parseFloat(point3[2]));
                            s=scanner.nextLine();

                            Shape t=new Triangle(Integer.parseInt(trans[1]),Integer.parseInt(mat[1]),p1,p2,p3);

                            scene.addShape(t);
                        }   break;
                }
            }
        }
        return scene;



    }
}
