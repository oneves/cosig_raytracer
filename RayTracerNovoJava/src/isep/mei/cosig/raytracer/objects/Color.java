package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Color {

    private double r;
    private double g;
    private double b;

    public Color(double r,double g, double b){
        this.r=r;
        this.g=g;
        this.b=b;
    }


    public void sumColor(Color c){
        this.r+=c.getColor()[0];
        this.g+=c.getColor()[1];
        this.b+=c.getColor()[2];
    }

    public static Color mulColor(Color c1, Color c2){
        double r=c1.getColor()[0]*c2.getColor()[0];
        double g=c1.getColor()[1]*c2.getColor()[1];
        double b=c1.getColor()[2]*c2.getColor()[2];
        return new Color(r,g,b);
    }

    public static Color mulColor(Color c, double m){
        return new Color(c.r*m,c.g*m,c.b*m);
    }

    public void setColor(double r,double g, double b){
        this.r=r;
        this.g=g;
        this.b=b;
    }
    public double[] getColor(){
        double[] color={r,g,b};
        return color;
    }

    public int getRGB(){
        int ir,ig,ib,color;

        ir=(int)(r*255);
        ig=(int)(g*255);
        ib=(int)(b*255);

        return color=(ir<<16)|(ig<<8)|ib;
    }
}
