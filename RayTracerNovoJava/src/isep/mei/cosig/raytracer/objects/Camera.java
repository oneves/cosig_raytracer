package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Camera {
    private int transformation;
    private float dist;
    private float field_of_view;

    public Camera(int t, float d,float fov){
        transformation=t;
        dist=d;
        field_of_view=fov;
    }

    public int getTransformation(){
        return transformation;
    }
    public float getDist(){
        return dist;
    }
    public float getFOV(){
        return field_of_view;
    }

    public void setTransformation(int transformation) {
        this.transformation = transformation;
    }

    public void setDist(float dist) {
        this.dist = dist;
    }

    public void setField_of_view(float field_of_view) {
        this.field_of_view = field_of_view;
    }


}
