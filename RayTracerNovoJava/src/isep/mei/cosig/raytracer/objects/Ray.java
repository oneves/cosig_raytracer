package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Ray {
    private Vector p0;
    private Vector dir;

    public Ray(Vector p0, Vector dir) {
        this.p0 = p0;
        this.dir = dir;
    }

    public Vector getP0() {
        return p0;
    }

    public Vector getDir() {
        return dir;
    }
}
