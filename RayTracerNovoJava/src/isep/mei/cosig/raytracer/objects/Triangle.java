package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Triangle extends Shape{

    private Vector p1;
    private Vector p2;
    private Vector p3;
    private Vector normal;


    public Triangle(int t, int m, Vector p1, Vector p2, Vector p3) {
        super(t, m);
        super.shape=0;
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;
        calcValues();
    }

    public void changeSpace(Vector x,Vector y,Vector z){
        p1=x;
        p2=y;
        p3=z;
    }

    private void calcValues(){
        normal=Vector.vProd(Vector.cVector(p1, p2),Vector.cVector(p1, p3)).normalize();

    }

    public Vector getP1() {
        return p1;
    }

    public Vector getP2() {
        return p2;
    }

    public Vector getP3() {
        return p3;
    }

    public Vector getNormal(){
        return normal;
    }
}
