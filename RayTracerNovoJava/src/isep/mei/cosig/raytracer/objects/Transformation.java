package isep.mei.cosig.raytracer.objects;

import org.ejml.simple.SimpleMatrix;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Transformation {
    private SimpleMatrix transformMatrix;
    private SimpleMatrix tranformInv;
    private double[] translate;
    private double[] rotation;

    public Transformation(){
        transformMatrix=SimpleMatrix.identity(4);
        translate=new double[3];
        rotation=new double[3];
    }

    public Transformation(Transformation t){
        this.transformMatrix=t.transformMatrix;
        this.tranformInv=t.tranformInv;
        this.rotation=t.rotation;
        this.translate=t.translate;
    }


    public void identityMatrix()
    {
        transformMatrix=SimpleMatrix.identity(4);

    }

    public void translate(double x, double y, double z)
    {
        double[][] translateMatrix=new double[4][4];
        translate[0]=x;
        translate[1]=y;
        translate[2]=z;

        translateMatrix[0][0] = 1.0;
        translateMatrix[0][1] = 0.0;
        translateMatrix[0][2] = 0.0;
        translateMatrix[0][3] = x;
        translateMatrix[1][0] = 0.0;
        translateMatrix[1][1] = 1.0;
        translateMatrix[1][2] = 0.0;
        translateMatrix[1][3] = y;
        translateMatrix[2][0] = 0.0;
        translateMatrix[2][1] = 0.0;
        translateMatrix[2][2] = 1.0;
        translateMatrix[2][3] = z;
        translateMatrix[3][0] = 0.0;
        translateMatrix[3][1] = 0.0;
        translateMatrix[3][2] = 0.0;
        translateMatrix[3][3] = 1.0;
//            SimpleMatrix transMat=new SimpleMatrix(translateMatrix);
//            transformMatrix=transMat.mult(transformMatrix);
        transformMatrix=transformMatrix.mult(new SimpleMatrix(translateMatrix));
//            multiply3(translateMatrix);
    }

    public void rotateX(double a)
    {

        double[][] rotateXMatrix=new double[4][4];
        rotation[0]=a;

        a *= Math.PI / 180.0;
        rotateXMatrix[0][0] = 1.0;
        rotateXMatrix[0][1] = 0.0;
        rotateXMatrix[0][2] = 0.0;
        rotateXMatrix[0][3] = 0.0;
        rotateXMatrix[1][0] = 0.0;
        rotateXMatrix[1][1] = Math.cos(a);
        rotateXMatrix[1][2] = Math.sin(a);
        rotateXMatrix[1][3] = 0.0;
        rotateXMatrix[2][0] = 0.0;
        rotateXMatrix[2][1] = -Math.sin(a);
        rotateXMatrix[2][2] = Math.cos(a);
        rotateXMatrix[2][3] = 0.0;
        rotateXMatrix[3][0] = 0.0;
        rotateXMatrix[3][1] = 0.0;
        rotateXMatrix[3][2] = 0.0;
        rotateXMatrix[3][3] = 1.0;
//            SimpleMatrix rotX=new SimpleMatrix(rotateXMatrix);
//            transformMatrix=rotX.mult(transformMatrix);
        transformMatrix=transformMatrix.mult(new SimpleMatrix(rotateXMatrix));
//            multiply3(rotateXMatrix);
    }

    public void rotateY(double a)
    {
        double[][] rotateYMatrix=new double[4][4];
        rotation[1]=a;

        a *= Math.PI / 180.0;
        rotateYMatrix[0][0] =  Math.cos(a);
        rotateYMatrix[0][1] =  0.0;
        rotateYMatrix[0][2] =  Math.sin(a);
        rotateYMatrix[0][3] =  0.0;

        rotateYMatrix[1][0] =  0.0;
        rotateYMatrix[1][1] =  1.0;
        rotateYMatrix[1][2] =  0.0;
        rotateYMatrix[1][3] =  0.0;

        rotateYMatrix[2][0] =  -Math.sin(a);
        rotateYMatrix[2][1] =  0.0;
        rotateYMatrix[2][2] =  Math.cos(a);
        rotateYMatrix[2][3] =  0.0;

        rotateYMatrix[3][0] =  0.0;
        rotateYMatrix[3][1] =  0.0;
        rotateYMatrix[3][2] =  0.0;
        rotateYMatrix[3][3] =  1.0;
//            SimpleMatrix rotY=new SimpleMatrix(rotateYMatrix);
//            transformMatrix=rotY.mult(transformMatrix);
        transformMatrix=transformMatrix.mult(new SimpleMatrix(rotateYMatrix));
    }

    public void rotateZ(double a)
    {
        double[][] rotateZMatrix=new double[4][4];
        rotation[2]=a;

        a *= Math.PI / 180.0;
        rotateZMatrix[0][0] =  Math.cos(a);
        rotateZMatrix[0][1] =  -Math.sin(a);
        rotateZMatrix[0][2] =  0.0;
        rotateZMatrix[0][3] =  0.0;

        rotateZMatrix[1][0] =  Math.sin(a);
        rotateZMatrix[1][1] =  Math.cos(a);
        rotateZMatrix[1][2] =  0.0;
        rotateZMatrix[1][3] =  0.0;

        rotateZMatrix[2][0] =  0.0;
        rotateZMatrix[2][1] =  0.0;
        rotateZMatrix[2][2] =  1.0;
        rotateZMatrix[2][3] =  0.0;

        rotateZMatrix[3][0] =  0.0;
        rotateZMatrix[3][1] =  0.0;
        rotateZMatrix[3][2] =  0.0;
        rotateZMatrix[3][3] =  1.0;
//            SimpleMatrix rotZ=new SimpleMatrix(rotateZMatrix);
//            transformMatrix=rotZ.mult(transformMatrix);
        transformMatrix=transformMatrix.mult(new SimpleMatrix(rotateZMatrix));
    }

    public void scale(double x, double y, double z)
    {
        double[][] scaleMatrix=new double[4][4];

        scaleMatrix[0][0] =  x;
        scaleMatrix[0][1] =  0.0;
        scaleMatrix[0][2] =  0.0;
        scaleMatrix[0][3] =  0.0;

        scaleMatrix[1][0] =  0.0;
        scaleMatrix[1][1] =  y;
        scaleMatrix[1][2] =  0.0;
        scaleMatrix[1][3] =  0.0;

        scaleMatrix[2][0] =  0.0;
        scaleMatrix[2][1] =  0.0;
        scaleMatrix[2][2] =  z;
        scaleMatrix[2][3] =  0.0;

        scaleMatrix[3][0] =  0.0;
        scaleMatrix[3][1] =  0.0;
        scaleMatrix[3][2] =  0.0;
        scaleMatrix[3][3] =  1.0;
//            SimpleMatrix sclMat=new SimpleMatrix(scaleMatrix);
//            transformMatrix=sclMat.mult(transformMatrix);
        transformMatrix=transformMatrix.mult(new SimpleMatrix(scaleMatrix));
    }

    public void invertMatrix(){
        tranformInv=transformMatrix.invert();
    }

    public Vector changeSpace(Vector v, boolean invert, int w){
        double[][] mC={{v.getX()},{v.getY()},{v.getZ()},{w}};
        SimpleMatrix matrixC;

        if (invert)
            matrixC=tranformInv.mult(new SimpleMatrix(mC));
        else
            matrixC=transformMatrix.mult(new SimpleMatrix(mC));

        double x=matrixC.get(0, 0);
        double y=matrixC.get(1, 0);
        double z=matrixC.get(2, 0);
        double wr=matrixC.get(3,0);

        Vector returnV;

        if (w==0)
            returnV= new Vector(x,y,z).normalize();
        else
            returnV=new Vector(x/wr,y/wr,z/wr);

        return returnV;
    }

    public Vector normalChangeSpace(Vector v){
        double[][] mC={{v.getX()},{v.getY()},{v.getZ()},{0}};
        SimpleMatrix matrixCt=tranformInv.transpose();
        SimpleMatrix matrixC=matrixCt.mult(new SimpleMatrix(mC));

        double x=matrixC.get(0, 0);
        double y=matrixC.get(1, 0);
        double z=matrixC.get(2, 0);

        return new Vector(x,y,z).normalize();
    }

    public void mult(Transformation t){
        transformMatrix=(t.tranformInv).mult(transformMatrix);
//        transformMatrix=transformMatrix.mult(t.transformMatrix);
        tranformInv=transformMatrix.invert();
    }

    public double[] getTranslate() {
        return translate;
    }

    public double[] getRotation() {
        return rotation;
    }

}
