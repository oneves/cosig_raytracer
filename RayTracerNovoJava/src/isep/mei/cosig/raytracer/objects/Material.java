package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Material {
    private Color color;
    private float ambient, diffuse, reflection, refraction, refracIndex;

    public Material(Color color, float ambient, float diffuse, float reflection, float refraction, float refracIndex) {
        this.color = color;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.reflection = reflection;
        this.refraction = refraction;
        this.refracIndex = refracIndex;
    }



    public Color getColor() {
        return color;
    }
    public float getAmbient() {
        return ambient;}
    public float getDiffuse() {return diffuse;
    }
    public float getReflection() {
        return reflection;
    }
    public float getRefraction() {
        return refraction;
    }
    public float getRefracIndex() {
        return refracIndex;
    }
}
