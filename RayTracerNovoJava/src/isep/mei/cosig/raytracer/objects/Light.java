package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Light {
    private int transformation;
    private Color color;

    public Light(int transformation, Color color) {
        this.transformation = transformation;
        this.color = color;
    }

    public int getTransformation() {
        return transformation;
    }
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
