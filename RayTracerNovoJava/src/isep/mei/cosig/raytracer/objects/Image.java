package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Image {

    private int h_res;
    private int v_res;
    private Color color;

    public Image(int h, int v, Color c){
        h_res=h;
        v_res=v;
        color=c;
    }

    public int getHRes(){return h_res;}
    public int getVRes(){return v_res;}
    public Color getColor(){return color;}

    public void setRes(int h,int v){h_res=h; v_res=v;}
}
