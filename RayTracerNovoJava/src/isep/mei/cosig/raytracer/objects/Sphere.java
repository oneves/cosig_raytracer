package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Sphere extends Shape {

    public Sphere(int t, int m) {
        super(t, m);
        super.shape=2;
    }
}
