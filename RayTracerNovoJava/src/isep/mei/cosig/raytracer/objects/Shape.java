package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Shape {

    protected int material;
    protected int transformation;
    protected char shape;//0-Triangle, 1-Box, 2-Sphere

    public Shape(int t,int m){
        this.material=m;
        this.transformation=t;
    }

    public int getMaterial() {
        return material;
    }

    public int getTransformation() {
        return transformation;
    }

    public char getShape() {
        return shape;
    }
}
