package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Intersection {

    Vector pInter;
    Vector p0;
    Vector normal;
    int mat;
    Double dist;

    public Intersection(Vector pInter,Vector p0, int mat,Vector normal) {
        this.pInter = pInter;
        this.mat=mat;
        this.p0=p0;
        this.normal=normal;
        this.dist=Vector.cVector(p0,pInter).nVector();
    }

    public Vector getpInter() {
        return pInter;
    }

    public int getMat() {
        return mat;
    }

    public Double getDist() {
        return dist;
    }

    public Vector getNormal(){
        return this.normal;
    }

}
