package isep.mei.cosig.raytracer.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Scene {
    private Camera cam;
    private Image imag;
    private List<Light> lights;
    private List<Transformation> trans;
    private List<Material> mats;
    private List<Shape> shapes;

    public Scene() {
        this.trans = new ArrayList<>();
        this.mats = new ArrayList<>();
        this.shapes = new ArrayList<>();
        this.lights=new ArrayList<>();
    }

    public Camera getCam() {
        return cam;
    }
    public Image getImag() {
        return imag;
    }
    public List<Transformation> getTrans() {
        return trans;
    }
    public Material getMats(int m) {
        return mats.get(m);
    }
    public List<Shape> getShapes() {
        return shapes;
    }
    public List<Light> getLights() {
        return lights;
    }

    public void addTransformation(Transformation T){
        trans.add(T);
    }
    public void editTransformation(Transformation T,int pos){
        trans.set(pos, T);
    }
    public void addMaterial(Material M){
        mats.add(M);
    }
    public void addShape(Shape S){
        shapes.add(S);
    }
    public void addLight(Light L){
        lights.add(L);
    }
    public void setCam(Camera cam) {
        this.cam = cam;
    }
    public void setImag(Image imag) {
        this.imag = imag;
    }
}
