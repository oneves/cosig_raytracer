package isep.mei.cosig.raytracer.objects;

/**
 * Created by Orlando Neves 1101166 | Francisco Fernandes 1150276 on 4/17/2016.
 */
public class Box extends Shape {
    public Box(int t, int m) {
        super(t, m);
        super.shape=1;
    }
}
