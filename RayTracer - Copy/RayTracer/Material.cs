﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Material
    {
        Color color;
        double ambient, diffuse, reflection, refraction;
        double index_refraction;


        public Material(Color color, double ambient, double diffuse, double reflection, 
            double refraction, double index_refraction)
        {
            this.color = color;
            this.ambient = ambient;
            this.diffuse = diffuse;
            this.reflection = reflection;
            this.refraction = refraction;
            this.index_refraction = index_refraction;
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public double Index_refraction
        {
            get { return index_refraction; }
            set
            {
                if (value < 0.1)
                {
                    index_refraction = 0.1;
                }
                else
                {
                    index_refraction = value;
                }
            }
        }

        public double Refraction
        {
            get { return refraction; }
            set
            {
                if (value < 0.0)
                {
                    refraction = 0.0;
                }
                else if (value > 1.0)
                {
                    refraction = 1.0;
                }
                else
                {
                    refraction = value;
                }
            }
        }

        public double Reflection
        {
            get { return reflection; }
            set
            {
                if (value < 0.0)
                {
                    reflection = 0.0;
                }
                else if (value > 1.0)
                {
                    reflection = 1.0;
                }
                else
                {
                    reflection = value;
                }
            }
        }

        public double Diffuse
        {
            get { return diffuse; }
            set
            {
                if (value < 0.0)
                {
                    diffuse = 0.0;
                }
                else if (value > 1.0)
                {
                    diffuse = 1.0;
                }
                else
                {
                    diffuse = value;
                }
            }
        }

        public double Ambient
        {
            get { return ambient; }
            set
            {
                if (value < 0.0)
                {
                    ambient = 0.0;
                }
                else if (value > 1.0)
                {
                    ambient = 1.0;
                }
                else
                {
                    ambient = value;
                }
            }
        }
    }
}
