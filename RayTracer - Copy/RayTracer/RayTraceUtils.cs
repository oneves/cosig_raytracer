﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class RayTraceUtils
    {

        static Camera camera;
        static Image image;
        static List<Light> lights;
        static List<Material> materials;
        static List<Triangle> triangles;

        static Triangle closestTriangle;
        static Position P;
        static double smallerT;

        internal static void rayTrace(Camera _camera, Image _image, List<Light> _lights, List<Material> _materials, List<Triangle> _triangles, int depth)
        {
            camera = _camera;
            image = _image;
            lights = _lights;
            materials = _materials;
            triangles = _triangles;

            closestTriangle = null;
            P = null;
            smallerT = double.MaxValue;

        }

        private double matrixDeterminenat3x3(double matrix00, double matrix01, double matrix02, double matrix10, double matrix11,
           double matrix12, double matrix20, double matrix21, double matrix22)
        {
            double sarrus = ((matrix00 * matrix11 * matrix22) + (matrix10 * matrix21 * matrix02) + (matrix01 * matrix12 * matrix20))
                - ((matrix02 * matrix11 * matrix20) + (matrix01 * matrix10 * matrix22) + (matrix12 * matrix21 * matrix00));

            return sarrus;
        }

        private double intersection(Ray ray, Triangle tri)
        {
            double gama, beta, t;

            //Ray position & direction
            Position rayPos = ray.Position;
            Vector rayDir = ray.Vector;

            //Triangle position
            Position pos1 = tri.V1;
            Position pos2 = tri.V2;
            Position pos3 = tri.V3;

            double detMatrixA = matrixDeterminenat3x3(pos1.X - pos2.X, pos1.X - pos3.X, rayDir.Position.X,
                                                      pos1.Y - pos2.Y, pos1.Y - pos3.Y, rayDir.Position.Y,
                                                      pos1.Z - pos2.Z, pos1.Z - pos3.Z, rayDir.Position.Z);

            double detB = matrixDeterminenat3x3(pos1.X - rayPos.X, pos1.X - pos3.X, rayDir.Position.X,
                                                pos1.Y - rayPos.Y, pos1.Y - pos3.Y, rayDir.Position.Y,
                                                pos1.Z - rayPos.Z, pos1.Z - pos3.Z, rayDir.Position.Z);

            beta = detB / detMatrixA;
            if (beta <= 0)
                return -1;

            double detY = matrixDeterminenat3x3(pos1.X - pos2.X, pos1.X - rayPos.X, rayDir.Position.X,
                                                pos1.Y - pos2.Y, pos1.Y - rayPos.Y, rayDir.Position.Y,
                                                pos1.Z - pos2.Z, pos1.Z - rayPos.Z, rayDir.Position.Z);

            gama = detY / detMatrixA;
            if (gama <= 0)
                return -1;
            if ((gama + beta) <= 1)
                return -1;

            double detT = matrixDeterminenat3x3(pos1.X - pos2.X, pos1.X - pos3.X, pos1.X - rayPos.X,
                                                pos1.Y - pos2.Y, pos1.Y - pos3.Y, pos1.Y - rayPos.Y,
                                                pos1.Z - pos2.Z, pos1.Z - pos3.Z, pos1.Y - rayPos.Z);

            t = detT / detMatrixA;
            if (t <= 0)
                return -1;
            if (t <= 10E-9)
                return -1;

            return t;
        }

        private bool shadow(Ray ray)
        {
            
            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle triangle = triangles[i];

                double t = intersection(ray, triangle);
                if (t != -1)
                {
                    if (t < lights[0].Position.Distance(P))
                    { }
                    return true;
                }
            }
            
            return false;
        }

        private bool closestTriangleIntersection(Ray ray)
        {
            
            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle triangle = triangles[i];

                double t = intersection(ray, triangle);
                if (t != -1)
                {
                    if (smallerT > t)
                    {
                        smallerT = t;
                        P = ray.Position + (ray.Vector * t);
                        closestTriangle = triangle;
                    }
                }
            }
            
            if (closestTriangle == null)
                return false;
            return true;
        }

        private Color rayTrace(Ray ray, int depth)
        {
            if (!closestTriangleIntersection(ray))
            {
                return image.BackgroundColor;
            }
            else
            {
                //black color
                Color color = new Color(0, 0, 0);
                Vector N = closestTriangle.Normal;

                //ambient light contribution
                int material_index = closestTriangle.Material;
                Material material = materials[material_index];

                foreach (Light light in lights)
                {
                    color = color + light.Color * material.Color * material.Ambient;
                }

                //diffuse light contribution
                foreach (Light light in lights)
                {
                    Vector L = (light.Position - P).Normalize();
                    Ray S = new Ray(P, L);
                    double cosB = N.Product(L);
                    if (!shadow(S) && cosB > 0)
                    {
                        color = color + light.Color * material.Color * material.Diffuse * cosB;
                    }
                }

                //specular light contribution

                return color;
            }
        }
    }
}
