﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Triangle
    {

        private int transformation, material;
        private Position v1, v2, v3;
        private Vector normal;

        public Triangle(int transformation, int material, Position v1, Position v2, Position v3) {
            this.transformation = transformation;
            this.material = material;
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }

        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public int Material
        {
            get { return material; }
            set { material = value; }
        }

        public Position V1
        {
            get { return v1; }
            set { v1 = value; }
        }

        public Position V2
        {
            get { return v2; }
            set { v2 = value; }
        }

        public Position V3
        {
            get { return v3; }
            set { v3 = value; }
        }

        internal Vector Normal
        {
            get { return normal; }
            set { normal = value; }
        }

        public void calNormal()
        {
            // determine the normal vector of a triangle and normalize it
            Vector _normal = ((V2 - V1) * (V3 - V1));

            Normal = _normal.Normalize();
        }
    }
}
