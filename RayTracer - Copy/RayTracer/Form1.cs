﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;

namespace RayTracer
{
    public partial class Form1 : Form
    {

        BackgroundWorker backgroundWorker = new BackgroundWorker();

        //Scene path 
        string path = @"Scene.txt";

        Camera camera;
        Image image;
        Transformation t;
        List<Transformation> transformations;
        List<Material> materials;
        List<Light> lights;
        List<Sphere> spheres;
        List<Box> boxes;
        List<Triangle> listTriangle = new List<Triangle>();
        Bitmap bmp;
        // Loaded
        bool loaded = false;




        public Form1()
        {
            InitializeComponent();

            // To report progress from the background worker we need to set this property
            backgroundWorker.WorkerReportsProgress = true;
            // This event will be raised on the worker thread when the worker starts
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            // This event will be raised when we call ReportProgress
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);

            bmp = new Bitmap(577, 312);
            camera = new Camera();
            t = new Transformation();


        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //tabControl1.SelectedIndexChanged += new EventHandler(tabControl1_SelectedIndexChanged);

            //  Enable the OpenGL depth testing functionality.
            GL.Enable(EnableCap.DepthTest);
            GL.DepthRange(-10000, 10000);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                progressBar1.Visible = true;
                button1.Enabled = false;
                path = openFileDialog1.FileName;
                loadDataTest();
                loaded = true;
                glControl1.Visible = true;
                glControl1.Invalidate();
                
            }
           
            //backgroundWorker.RunWorkerAsync();

        }



        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //loadConfiguration(path);
            // get a reference to the worker that started this request
            BackgroundWorker workerSender = sender as BackgroundWorker;

            string[] lines = System.IO.File.ReadAllLines(@path);

            // List of readable values
            List<string> values;

            // Initialize variables
            transformations = new List<Transformation>();
            materials = new List<Material>();
            lights = new List<Light>();
            spheres = new List<Sphere>();
            boxes = new List<Box>();
            int loading = 0;
            double auxLoading=0;

            for (int i = 0; i < lines.Length; i++)
            {
                switch (lines[i])
                {
                    case "Camera":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 3; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            int transformationIndex = Convert.ToInt32(values[0]);
                            double distance = double.Parse(values[1], CultureInfo.InvariantCulture);
                            double fieldOfView = double.Parse(values[2], CultureInfo.InvariantCulture);
                            Transformation transf = transformations[transformationIndex];
                            Position p = new Position(transf.TX, transf.TY, transf.TZ);
                            camera = new Camera(distance, fieldOfView, p, transf.RX, transf.RZ);
                        }

                        break;

                    case "Image":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            string[] location = values[0].Split(' ');
                            int horizontal = Convert.ToInt32(location[0]);
                            int vertical = Convert.ToInt32(location[1]);
                            //
                            string[] colorValues = values[1].Split('\t')[1].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));
                            //
                            image = new Image(color, horizontal, vertical);
                        }

                        break;

                    case "Light":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Color
                            string[] colorValues = values[1].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));

                            //Add new light
                            lights.Add(new Light(transformationIndex, color));
                        }
                        break;

                    case "Material":
                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            string[] colorValues = values[0].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));
                            //
                            string[] status = values[1].Split(' ');
                            double ambient = double.Parse(status[0], CultureInfo.InvariantCulture);
                            double diffuse = double.Parse(status[1], CultureInfo.InvariantCulture);
                            double reflection = double.Parse(status[2], CultureInfo.InvariantCulture);
                            double refraction = double.Parse(status[3], CultureInfo.InvariantCulture);
                            double index_refraction = double.Parse(status[4], CultureInfo.InvariantCulture);
                            //
                            materials.Add(new Material(color, ambient, diffuse, reflection, refraction, index_refraction));
                        }
                        break;

                    case "Triangles":
                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        while (lines[i] != "}")
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Getting the transformation index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Removing transformation index from values
                            values.RemoveAt(0);
                            //Getting total triangles number
                            int numberOfTriangles = (values.Count - 1) / 4;
                            //
                            //List<Triangle> listTriangle = new List<Triangle>();
                            //
                            for (int k = 0; k < numberOfTriangles; k++)
                            {
                                int index = 4 * k;
                                //
                                int material = Convert.ToInt32(values[index]);
                                index++;
                                //
                                string[] coordinates = values[index].Split(' ');
                                double x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                double y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                double z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v1 = new Position(x, y, z);
                                index++;
                                //
                                coordinates = values[index].Split(' ');
                                x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v2 = new Position(x, y, z);
                                index++;
                                //
                                coordinates = values[index].Split(' ');
                                x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v3 = new Position(x, y, z);
                                //
                                Triangle triangle = new Triangle(transformationIndex, material, v1, v2, v3);
                                listTriangle.Add(triangle);
                            }
                        }
                        break;

                    case "Box":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Material
                            int materialIndex = Convert.ToInt32(values[1]);

                            //Add new light
                            boxes.Add(new Box(transformationIndex, materialIndex));
                        }
                        break;

                    case "Sphere":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Material
                            int materialIndex = Convert.ToInt32(values[1]);

                            //Add new light
                            spheres.Add(new Sphere(transformationIndex, materialIndex));
                        }
                        break;

                   case "Transformation":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        while (lines[i] != "}")
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        Transformation t = new Transformation(0,0,0,0,0,0,0,0,0);

                        for (int j = 0; j < values.Count; j++)
                        {
                            String [] lineValues = values[j].Split(' ');

                            switch (lineValues[0].Split('\t')[1])
                            {
                                case "T":

                                    t.TX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    t.TY = double.Parse(lineValues[2], CultureInfo.InvariantCulture);
                                    t.TZ = double.Parse(lineValues[3], CultureInfo.InvariantCulture);
                                    break;

                                case "Rx":
                                    t.RX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "Ry":
                                    t.RY = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "Rz":
                                    t.RZ = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "S":
                                    t.SX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    t.SY = double.Parse(lineValues[2], CultureInfo.InvariantCulture);
                                    t.SZ = double.Parse(lineValues[3], CultureInfo.InvariantCulture);
                                    break;
                                default: break;
                            }
                        }

                        // Pass the last useless line
                        i++;

                        transformations.Add(t);
                        break;
                    default:
                        break;
                }
                auxLoading = ((double)i*100.0)/(double)lines.Length;
                loading = Convert.ToInt32(auxLoading);
                workerSender.ReportProgress(loading);
            }

            int jj = transformations.Count;
            

        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            progressBar1.Value = e.ProgressPercentage;
        }

        public void sceneTrace()
        {
            Transformation t = new Transformation();
            t.transform(camera.Position.X, camera.Position.Y, camera.Position.Z, camera.Horizontal, camera.Vertical);

          
             for (int i = 0; i < listTriangle.Count; i++)
                {
                    Triangle triangle = listTriangle[i];

                    triangle.V1 = t.transform_point(triangle.V1);
                    triangle.V2 = t.transform_point(triangle.V2);
                    triangle.V3 = t.transform_point(triangle.V3);

                    triangle.calNormal();
                }

            Light light = lights[0];
            
            Position posi = new Position(transformations[light.Transformation].TX, transformations[light.Transformation].TY, transformations[light.Transformation].TZ);
            light.Position = t.transform_point(posi);

            RayTraceUtils.rayTrace(camera, image, lights, materials, listTriangle, Convert.ToInt32(numericUpDown1.Value));
        }

        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Enabled = true; // enable button

        }

        

        private void loadDataTest()
        {
            //loadConfiguration(path);
            // get a reference to the worker that started this request
            //BackgroundWorker workerSender = sender as BackgroundWorker;

            string[] lines = System.IO.File.ReadAllLines(@path);

            // List of readable values
            List<string> values;

            // Initialize variables
            transformations = new List<Transformation>();
            materials = new List<Material>();
            lights = new List<Light>();
            spheres = new List<Sphere>();
            boxes = new List<Box>();
            int loading = 0;
            double auxLoading = 0;

            for (int i = 0; i < lines.Length; i++)
            {
                switch (lines[i])
                {
                    case "Camera":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 3; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            int transformationIndex = Convert.ToInt32(values[0]);
                            double distance = double.Parse(values[1], CultureInfo.InvariantCulture);
                            double fieldOfView = double.Parse(values[2], CultureInfo.InvariantCulture);
                            Transformation transf = transformations[transformationIndex];
                            Position p = new Position(transf.TX, transf.TY, transf.TZ);
                            camera = new Camera(distance, fieldOfView, p, transf.RX, transf.RZ);
                        }

                        break;

                    case "Image":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            string[] location = values[0].Split(' ');
                            int horizontal = Convert.ToInt32(location[0]);
                            int vertical = Convert.ToInt32(location[1]);
                            //
                            string[] colorValues = values[1].Split('\t')[1].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));
                            //
                            image = new Image(color, horizontal, vertical);
                        }

                        break;

                    case "Light":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Color
                            string[] colorValues = values[1].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));

                            //Add new light
                            lights.Add(new Light(transformationIndex, color));
                        }
                        break;

                    case "Material":
                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            string[] colorValues = values[0].Split(' ');
                            Color color = new Color(double.Parse(colorValues[0], CultureInfo.InvariantCulture), double.Parse(colorValues[1], CultureInfo.InvariantCulture), double.Parse(colorValues[2], CultureInfo.InvariantCulture));
                            //
                            string[] status = values[1].Split(' ');
                            double ambient = double.Parse(status[0], CultureInfo.InvariantCulture);
                            double diffuse = double.Parse(status[1], CultureInfo.InvariantCulture);
                            double reflection = double.Parse(status[2], CultureInfo.InvariantCulture);
                            double refraction = double.Parse(status[3], CultureInfo.InvariantCulture);
                            double index_refraction = double.Parse(status[4], CultureInfo.InvariantCulture);
                            //
                            materials.Add(new Material(color, ambient, diffuse, reflection, refraction, index_refraction));
                        }
                        break;

                    case "Triangles":
                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        while (lines[i] != "}")
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Getting the transformation index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Removing transformation index from values
                            values.RemoveAt(0);
                            //Getting total triangles number
                            int numberOfTriangles = values.Count / 4;
                            //
                            //List<Triangle> listTriangle = new List<Triangle>();
                            //
                            for (int k = 0; k < numberOfTriangles; k++)
                            {
                                int index = 4 * k;
                                //
                                int material = Convert.ToInt32(values[index]);
                                index++;
                                //
                                string[] coordinates = values[index].Split(' ');
                                double x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                double y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                double z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v1 = new Position(x, y, z);
                                index++;
                                //
                                coordinates = values[index].Split(' ');
                                x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v2 = new Position(x, y, z);
                                index++;
                                //
                                coordinates = values[index].Split(' ');
                                x = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                                y = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
                                z = double.Parse(coordinates[2], CultureInfo.InvariantCulture);
                                Position v3 = new Position(x, y, z);
                                //
                                Triangle triangle = new Triangle(transformationIndex, material, v1, v2, v3);
                                listTriangle.Add(triangle);
                            }
                        }
                        break;

                    case "Box":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Material
                            int materialIndex = Convert.ToInt32(values[1]);

                            //Add new light
                            boxes.Add(new Box(transformationIndex, materialIndex));
                        }
                        break;

                    case "Sphere":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        for (int j = 0; j < 2; j++)
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        // Pass the last useless line
                        i++;

                        // Parse values into new object
                        {
                            //Transformation Index
                            int transformationIndex = Convert.ToInt32(values[0]);
                            //Material
                            int materialIndex = Convert.ToInt32(values[1]);

                            //Add new light
                            spheres.Add(new Sphere(transformationIndex, materialIndex));
                        }
                        break;

                    case "Transformation":

                        // Get the values
                        values = new List<string>();

                        // Get of the label line and pass the first useless line
                        i += 2;

                        // Cycle for the values
                        while (lines[i] != "}")
                        {
                            // Read the values
                            values.Add(lines[i]);
                            i++;
                        }

                        Transformation t = new Transformation(0, 0, 0, 0, 0, 0, 0, 0, 0);

                        for (int j = 0; j < values.Count; j++)
                        {
                            String[] lineValues = values[j].Split(' ');

                            switch (lineValues[0].Split('\t')[1])
                            {
                                case "T":

                                    t.TX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    t.TY = double.Parse(lineValues[2], CultureInfo.InvariantCulture);
                                    t.TZ = double.Parse(lineValues[3], CultureInfo.InvariantCulture);
                                    break;

                                case "Rx":
                                    t.RX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "Ry":
                                    t.RY = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "Rz":
                                    t.RZ = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    break;

                                case "S":
                                    t.SX = double.Parse(lineValues[1], CultureInfo.InvariantCulture);
                                    t.SY = double.Parse(lineValues[2], CultureInfo.InvariantCulture);
                                    t.SZ = double.Parse(lineValues[3], CultureInfo.InvariantCulture);
                                    break;
                                default: break;
                            }
                        }

                        // Pass the last useless line
                        i++;

                        transformations.Add(t);
                        break;
                    default:
                        break;
                }
                auxLoading = ((double)i * 100.0) / (double)lines.Length;
                loading = Convert.ToInt32(auxLoading);
                //workerSender.ReportProgress(loading);
            }

            int jj = transformations.Count;

        }

        void glControl1_Resize(object sender, EventArgs e)
        {

            glControl1.Validate();
        }

        private void SetupViewport()
        {
            double w = glControl1.Width;
            double h = glControl1.Height;

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            double zNear = 0.1;
            double zFar = 400;
            double aspect = w / h;
            double fH = Math.Tan(camera.FieldOfView * (Math.PI / 180)) * zNear;
            double fW = fH * aspect;
            GL.Frustum(-fW, fW, -fH, fH, zNear, zFar);
        }

        private void glControl1_Paint(object sender, PaintEventArgs e) {
            if (!loaded)
                return;

            glControl1.MakeCurrent();
            GL.ClearColor((float)image.BackgroundColor.R, (float)image.BackgroundColor.G, (float)image.BackgroundColor.B, 1);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.LoadIdentity();

            SetupViewport();

            t.transform(camera.Position.X, camera.Position.Y, camera.Position.Z, camera.Horizontal, camera.Vertical);

            // Draw triangles
            GL.PushMatrix();

            GL.Begin(PrimitiveType.Triangles);

            for (int i = 0; i < listTriangle.Count; i++)
            {
                Triangle tri = listTriangle[i];
                Material mat = materials[tri.Material];

                GL.Color3(mat.Color.R, mat.Color.G, mat.Color.B);
                Position trnPosition = t.transform_point(tri.V1);
                GL.Vertex3(trnPosition.X, trnPosition.Y, trnPosition.Z);

                trnPosition = t.transform_point(tri.V2);
                GL.Vertex3(trnPosition.X, trnPosition.Y, trnPosition.Z);

                trnPosition = t.transform_point(tri.V3);
                GL.Vertex3(trnPosition.X, trnPosition.Y, trnPosition.Z);
            }

            GL.End();
            GL.PopMatrix();

            GL.Flush();
            glControl1.SwapBuffers();
            glControl1.DrawToBitmap
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //progressBar1.Increment(+1);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            sceneTrace();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //W - 577
            //H - 312
        }

        private void Local_Click(object sender, EventArgs e)
        {

        }

        private void glControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
