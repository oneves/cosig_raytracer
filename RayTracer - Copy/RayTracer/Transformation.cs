﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Transformation
    {
        //Translation
        double tx, ty, tz;
        //Rotation
        double rx, ry, rz;
        //Scale
        double sx, sy, sz;

        double[,] transformMatrix = new double[4, 4];

        Position position;

        public Transformation()
        {

        }

        public Transformation(double tx, double ty, double tz, double rx, double ry, double rz,
            double sx, double sy, double sz)
        {
            this.tx = tx;
            this.ty = ty;
            this.tz = tz;
            this.rx = rx;
            this.ry = ry;
            this.rz = rz;
            this.sx = sx;
            this.sy = sy;
            this.sz = sz;
            Position pos = new Position(tx, ty,tz);
            
        }

        public double TX
        {
            get { return tx; }
            set { tx = value; }
        }

        public double TY
        {
            get { return ty; }
            set { ty = value; }
        }

        public double TZ
        {
            get { return tz; }
            set { tz = value; }
        }

        public double RX
        {
            get { return rx; }
            set { rx = value; }
        }

        public double RY
        {
            get { return ry; }
            set { ry = value; }
        }

        public double RZ
        {
            get { return rz; }
            set { rz = value; }
        }

        public double SX
        {
            get { return sx; }
            set { sx = value; }
        }

        public double SY
        {
            get { return sy; }
            set { sy = value; }
        }

        public double SZ
        {
            get { return sz; }
            set { sz = value; }
        }
        void multiply1(double[] pointA, double[] pointB)
        {
            int i, j;

            for (i = 0; i < 4; i++)
                pointB[i] = 0.0;
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    pointB[i] += transformMatrix[i, j] * pointA[j];
        }

        void multiply3(double[,] matrixA)
        {
            int i, j, k;
            double[,] matrixB = new double[4, 4];

            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                {
                    matrixB[i, j] = transformMatrix[i, j];
                    transformMatrix[i, j] = 0.0;
                }
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    for (k = 0; k < 4; k++)
                        transformMatrix[i, j] += matrixA[i, k] * matrixB[k, j];
        }

        public void identityMatrix()
        {
            transformMatrix[0, 0] = 1.0;
            transformMatrix[0, 1] = 0.0;
            transformMatrix[0, 2] = 0.0;
            transformMatrix[0, 3] = 0.0;
            transformMatrix[1, 0] = 0.0;
            transformMatrix[1, 1] = 1.0;
            transformMatrix[1, 2] = 0.0;
            transformMatrix[1, 3] = 0.0;
            transformMatrix[2, 0] = 0.0;
            transformMatrix[2, 1] = 0.0;
            transformMatrix[2, 2] = 1.0;
            transformMatrix[2, 3] = 0.0;
            transformMatrix[3, 0] = 0.0;
            transformMatrix[3, 1] = 0.0;
            transformMatrix[3, 2] = 0.0;
            transformMatrix[3, 3] = 1.0;
        }

        void translate(double x, double y, double z)
        {
            double[,] translateMatrix = new double[4, 4];

            translateMatrix[0, 0] = 1.0;
            translateMatrix[0, 1] = 0.0;
            translateMatrix[0, 2] = 0.0;
            translateMatrix[0, 3] = x;
            translateMatrix[1, 0] = 0.0;
            translateMatrix[1, 1] = 1.0;
            translateMatrix[1, 2] = 0.0;
            translateMatrix[1, 3] = y;
            translateMatrix[2, 0] = 0.0;
            translateMatrix[2, 1] = 0.0;
            translateMatrix[2, 2] = 1.0;
            translateMatrix[2, 3] = z;
            translateMatrix[3, 0] = 0.0;
            translateMatrix[3, 1] = 0.0;
            translateMatrix[3, 2] = 0.0;
            translateMatrix[3, 3] = 1.0;
            multiply3(translateMatrix);
        }

        void rotateX(double a)
        {
            double[,] rotateXMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateXMatrix[0, 0] = 1.0;
            rotateXMatrix[0, 1] = 0.0;
            rotateXMatrix[0, 2] = 0.0;
            rotateXMatrix[0, 3] = 0.0;
            rotateXMatrix[1, 0] = 0.0;
            rotateXMatrix[1, 1] = Math.Cos(a);
            rotateXMatrix[1, 2] = Math.Sin(a);
            rotateXMatrix[1, 3] = 0.0;
            rotateXMatrix[2, 0] = 0.0;
            rotateXMatrix[2, 1] = -Math.Sin(a);
            rotateXMatrix[2, 2] = Math.Cos(a);
            rotateXMatrix[2, 3] = 0.0;
            rotateXMatrix[3, 0] = 0.0;
            rotateXMatrix[3, 1] = 0.0;
            rotateXMatrix[3, 2] = 0.0;
            rotateXMatrix[3, 3] = 1.0;
            multiply3(rotateXMatrix);
        }

        void rotateY(double a)
        {
            double[,] rotateYMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateYMatrix[0, 0] = Math.Cos(a);
            rotateYMatrix[0, 1] = 0.0;
            rotateYMatrix[0, 2] = Math.Sin(a);
            rotateYMatrix[0, 3] = 0.0;
            rotateYMatrix[1, 0] = 0.0;
            rotateYMatrix[1, 1] = 1.0;
            rotateYMatrix[1, 2] = 0.0;
            rotateYMatrix[1, 3] = 0.0;
            rotateYMatrix[2, 0] = -Math.Sin(a);
            rotateYMatrix[2, 1] = 0.0;
            rotateYMatrix[2, 2] = Math.Cos(a);
            rotateYMatrix[2, 3] = 0.0;
            rotateYMatrix[3, 0] = 0.0;
            rotateYMatrix[3, 1] = 0.0;
            rotateYMatrix[3, 2] = 0.0;
            rotateYMatrix[3, 3] = 1.0;
            multiply3(rotateYMatrix);
        }

        void rotateZ(double a)
        {
            double[,] rotateZMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateZMatrix[0, 0] = Math.Cos(a);
            rotateZMatrix[0, 1] = -Math.Sin(a);
            rotateZMatrix[0, 2] = 0.0;
            rotateZMatrix[0, 3] = 0.0;
            rotateZMatrix[1, 0] = Math.Sin(a);
            rotateZMatrix[1, 1] = Math.Cos(a);
            rotateZMatrix[1, 2] = 0.0;
            rotateZMatrix[1, 3] = 0.0;
            rotateZMatrix[2, 0] = 0.0;
            rotateZMatrix[2, 1] = 0.0;
            rotateZMatrix[2, 2] = 1.0;
            rotateZMatrix[2, 3] = 0.0;
            rotateZMatrix[3, 0] = 0.0;
            rotateZMatrix[3, 1] = 0.0;
            rotateZMatrix[3, 2] = 0.0;
            rotateZMatrix[3, 3] = 1.0;
            multiply3(rotateZMatrix);
        }

        public void transform(double x, double y, double z, double horizontal, double vertical)
        {
            identityMatrix();
            rotateZ(horizontal); // 45
            rotateX(vertical); // 60 
            translate(x, y, z); // x, y, z
        }

        public Position transform_point(Position p)
        {
            Position new_p = new Position(0, 0, 0);
            double[] pointA = new double[4];
            double[] pointB = new double[4];

            pointA[0] = p.X;
            pointA[1] = p.Y;
            pointA[2] = p.Z;
            pointA[3] = 1.0;

            multiply1(pointA, pointB);

            new_p.X = pointB[0] / pointB[3];
            new_p.Y = pointB[1] / pointB[3];
            new_p.Z = pointB[2] / pointB[3];

            return new_p;
        }
    }
}
