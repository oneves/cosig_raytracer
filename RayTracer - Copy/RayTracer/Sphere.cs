﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Sphere
    {
        private int transformation;
        private int material;

        public Sphere(int transformation, int material) {
            this.transformation = transformation;
            this.material = material;

        }

        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public int Material
        {
            get { return material; }
            set { material = value; }
        }

    }
}
