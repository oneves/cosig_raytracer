﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Vector
    {

        Position position;

        public Vector(Position position)
        {
            this.position = position;
        }

        public Position Position
        {
            get { return position; }
            set { position = value; }
        }

        static public Vector operator *(Vector p1, Vector p2)
        {
            return new Vector(new Position(
                p1.Position.Y * p2.Position.Z - p1.Position.Z * p2.Position.Y,
                p1.Position.Z * p2.Position.X - p1.Position.X * p2.Position.Z,
                p1.Position.X * p2.Position.Y - p1.Position.Y * p2.Position.X));
        }

        static public Vector operator *(Vector vec, double value)
        {
            return new Vector(new Position(vec.Position.X * value, vec.Position.Y * value, vec.Position.Z * value));
        }

        public Vector Normalize()
        {
            //magnitude (or absolute value) of a vector is its length
            double length = Math.Sqrt((position.X * position.X) + (position.Y * position.Y) + (position.Z * position.Z));

            return new Vector(new Position(position.X / length, position.Y / length, position.Z / length));
        }

        public double Product(Vector p)
        {
            return Position.X * p.Position.X + Position.Y * p.Position.Y + Position.Z * p.Position.Z;
        }

    }
}
