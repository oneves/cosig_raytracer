﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Ray
    {
        Position position;
        Vector vector;

        public Ray(Position position, Vector vector)
        {
            this.position = position;
            this.vector = vector;
        }

        public Vector Vector
        {
            get { return vector; }
            set { vector = value; }
        }

        public Position Position
        {
            get { return position; }
            set { position = value; }
        }
    }
}
