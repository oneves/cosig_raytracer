﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Camera
    {
        private double distance, field_of_view;
        private int transformation;
        Position position;
        double horizontal, vertical;

        public Camera(int transformation, double dist, double field_of_view)
        {
            this.transformation = transformation;
            this.distance = dist;
            this.field_of_view = field_of_view;
        }

        public Camera()
        {
            this.transformation = 1;
            this.distance = 0;
            this.field_of_view = 0;
            this.position = new Position(0, 0, 0);
        }

        public Camera(double distance, double field_of_view, Position position, double horizontal, double vertical)
        {
            this.distance = distance;
            this.field_of_view = field_of_view;
            this.position = position;
            this.horizontal = horizontal;
            this.vertical = vertical;
        }


        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public double Distance
        {
            get { return distance; }
            set {
                if (value >= 0.1)
                    distance = value;
                else distance = 0.1;
            }
        }

        public double FieldOfView
        {
            get { return field_of_view; }
            set {
                if (value >= 0.1)
                    field_of_view = value;
                else field_of_view = 0.1;
            }
        }

        public Position Position
        {
            get { return position; }
            set { position = value; }
        }
        
        public double Vertical
        {
            get { return vertical; }
            set { vertical = value; }
        }

        public double Horizontal
        {
            get { return horizontal; }
            set { horizontal = value; }
        }
    }
}
