﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Position
    {
        double x, y, z;

        public Position(double x, double y,  double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double Z
        {
            get { return z; }
            set { z = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Distance(Position p2)
        {
            Double distance = Math.Sqrt((X - p2.X) * (X - p2.X) + (Y - p2.Y) * (Y - p2.Y) + (Z - p2.Z) * (Z - p2.Z));
            return distance;
        }

        public static Vector operator -(Position p1, Position p2)
        {
            Position pos = new Position(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
            return new Vector(pos);
        }

        static public Position operator +(Position p, Vector vec)
        {
            return new Position(p.X + vec.Position.X, p.Y + vec.Position.Y, p.Z + vec.Position.Z);
        }
    } 
}
