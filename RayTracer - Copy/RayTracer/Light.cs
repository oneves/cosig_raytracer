﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Light
    {

        Color color;
        private int transformation;
        Position position;

        public Light(int transformation, Color color)
        {
            this.color = color;
            this.transformation = transformation;
        }

        public int Transformation
        {
            get { return transformation; }
            set { transformation = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }


        public Position Position
        {
            get { return position; }
            set { position = value; }
        }
    }
}
