﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayTracer
{
    class Color
    {
        double r, g, b;

        public Color(double r, double g, double b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public double R
        {
            get { return r; }
            set
            {
                if (value < 0.0)
                {
                    r = 0.0;
                }
                else if (value > 1.0)
                {
                    r = 1.0;
                }
                else
                {
                    r = value;
                }
            }
        }

        public double G
        {
            get { return g; }
            set
            {
                if (value < 0.0)
                {
                    g = 0.0;
                }
                else if (value > 1.0)
                {
                    g = 1.0;
                }
                else
                {
                    g = value;
                }
            }
        }

        public double B
        {
            get { return b; }
            set
            {
                if (value < 0.0)
                {
                    b = 0.0;
                }
                else if (value > 1.0)
                {
                    b = 1.0;
                }
                else
                {
                    b = value;
                }
            }
        }

        static public Color operator *(Color c1, Color c2)
        {
            return new Color(c1.R * c2.R, c1.G * c2.G, c1.B * c2.B);
        }

        static public Color operator +(Color c1, Color c2)
        {
            return new Color(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B);
        }

        static public Color operator *(Color c, Double value)
        {
            return new Color(c.R * value, c.G * value, c.B * value);
        }
    }
}
